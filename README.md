# fsim

Radio camera F-engine simulator

## Build guidance

- Minimum [CMake](https://cmake.org) version: 3.23
- C++ standard: 20
  - tested with [gcc](https://gcc.gnu.org) v12.1.0
- Dependencies
  - [bark](https://gitlab.com/dsa-2000/rcp/bark), minimum
    version 0.100.0
  - [librcp](https://gitlab.com/dsa-2000/rcp/librcp), minimum
    version 0.107.0
  - [rcpdc](https://gitlab.com/dsa-2000/rcp/rcpdc), minimum version
    0.106.0

## Spack package

[Spack](https://spack.io) package is available in the DSA-2000 Spack
[package repository](https://gitlab.com/dsa-2000/code/spack).

## Getting involved

The project web site may be found at
https://gitlab.com/dsa-2000/rcp/fsim. Instructions for cloning the
code repository can be found on that site. To provide feedback (*e.g*
bug reports or feature requests), please see the issue tracker on that
site. If you wish to contribute to this project, please read
[CONTRIBUTING.md](CONTRIBUTING.md) for guidelines.
