// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "CDFInverseRegion.hpp"
#include "Configuration.hpp"
#include "fsim.hpp"

#include <rcp/FSamplesRegion.hpp>

#include <random>

namespace rcp::fsim {

/*! \brief task launcher to initialize F-engine sample values */
class InitFSamples {
public:
  /*! \brief rng seed type alias*/
  using seed_t = int;

  /*! \brief task argument type */
  struct Args {
    /*! \brief rng seed */
    seed_t seed;
  };

private:
  /*! \brief FSamplesRegion type alias */
  using md_t = FSamplesRegion;

  /*! \brief CDFInverseRegion type alias */
  using icdf_t = CDFInverseRegion;

  /*! \brief task launcher */
  L::IndexTaskLauncher m_launcher;

  /*! \brief partition of sample region index space */
  md_t::index_partition_t m_ip;

  /*! \brief task arguments */
  Args m_args;

  /*! \brief rng for rng seeds passed to task launches */
  std::mt19937 m_gen;

  /*! \brief rng seed distribution */
  std::uniform_int_distribution<seed_t> m_distrib;

public:
  /*! \brief default constructor (defaulted) */
  InitFSamples() = default;

  /*! \brief constructor */
  InitFSamples(
    L::Context ctx,
    L::Runtime* rt,
    Configuration const& config,
    md_t::index_space_t index_space,
    receivers_def_t const& receivers_by_shard,
    unsigned receiver_block_size,
    seed_t init_rng_seed,
    L::MappingTagID tag = 0);

  /*! \brief copy constructor (defaulted) */
  InitFSamples(InitFSamples const&) = default;

  /*! \brief move constructor (defaulted) */
  InitFSamples(InitFSamples&&) = default;

  /*! \brief destructor (defaulted) */
  ~InitFSamples() = default;

  /*! \brief copy assignment (defaulted) */
  auto
  operator=(InitFSamples const&) -> InitFSamples& = default;

  /* \brief move assignment (defaulted) */
  auto
  operator=(InitFSamples&&) -> InitFSamples& = default;

  /*! \brief launch task to initialize sample values using samples
   * distribution */
  auto
  launch(
    L::Context ctx,
    L::Runtime* rt,
    md_t::LogicalRegion samples,
    icdf_t::LogicalRegion inverse_cdf) -> void;

  /*! \brief preregister the task with a given task id */
  static auto
  preregister(L::TaskID id) -> void;
};
} // namespace rcp::fsim

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
