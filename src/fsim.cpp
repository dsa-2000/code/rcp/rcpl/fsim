// Copyright 2023,2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent

#include "fsim.hpp"
#include "Configuration.hpp"
#include "InitFSamples.hpp"
#include "InitSampleDistributions.hpp"
#include "SendFSamples.hpp"

#include <rcp/FSamplesRegion.hpp>
#include <rcp/GetConfig.hpp>
#include <rcp/GetEpoch.hpp>
#include <rcp/Mapper.hpp>
#include <rcp/rcp.hpp>

#include <vector>

namespace rcp::fsim {

auto
samples_layout() -> PortableLayout<> const& {
  using md_t = FSamplesRegion;
  static PortableLayout<> result{
    {{LayoutStyle::CPU,
      [](L::LayoutConstraintRegistrar& registrar) {
        registrar.add_constraint(L::OrderingConstraint(
          {L::DimensionKind(md_t::axes_t::timestep),
           L::DimensionKind(md_t::axes_t::polarization),
           L::DimensionKind(md_t::axes_t::channel),
           L::DimensionKind(md_t::axes_t::receiver),
           LEGION_DIM_F},
          true /* contiguous */));
      }},
     {LayoutStyle::GPU,
      [](L::LayoutConstraintRegistrar& registrar) {
        registrar.add_constraint(L::OrderingConstraint(
          {L::DimensionKind(md_t::axes_t::receiver),
           L::DimensionKind(md_t::axes_t::channel),
           L::DimensionKind(md_t::axes_t::polarization),
           L::DimensionKind(md_t::axes_t::timestep),
           LEGION_DIM_F},
          true /* contiguous */));
      }}},
    "fsamples layout"};
  return result;
}

LogSink<Tag> log_fsim;

auto
init_log_sink(std::string_view const& name) -> void {
  log_fsim = LogSink<Tag>(name);
}

L::TaskID fsim_task_id = 0;
using GetConfig = GetConfig_<ConfigurationSerdez, 1>;
using GetEpoch = GetEpoch_<2>;
L::TaskID constexpr init_fsamples_task_id = 3;
L::TaskID constexpr send_fsamples_task_id = 4;
L::TaskID constexpr init_sample_distributions_task_id = 5;

void
fsim_task(
  L::Task const* task,
  std::vector<L::PhysicalRegion> const&,
  L::Context ctx,
  L::Runtime* rt) {

  assert([&]() -> bool { return task->get_shard_domain().get_dim() == 1; }());
  auto shard_point = task->get_shard_point()[0];
  Configuration config;
  {
    GetConfig get_config(ctx, rt);
    config = get_config.launch(ctx, rt);
    get_config.destroy(ctx, rt);
  }
  if (shard_point == 0 && config.do_show())
    std::cout << config << '\n';
  if (!config.do_run())
    return;
  config.normalize_pmfs();

  auto receivers_by_shard = rcp::block_distribute(
    rcp::dc::receiver_type(DataPacketConfiguration::num_receivers),
    rcp::dc::receiver_type(task->get_total_shards()));

  using namespace std::chrono_literals;

  // FSendRegionGenerator fsend_generator(ctx, rt, config);
  auto gen = DenseArrayRegionGenerator<FSamplesRegion>(
    ctx,
    rt,
    {{FSamplesRegion::axes_t::timestep,
      IndexInterval<FSamplesRegion::coord_t>::right_bounded(
        config.num_send_timesteps() - 1)},
     {FSamplesRegion::axes_t::channel,
      IndexInterval<FSamplesRegion::coord_t>::right_bounded(
        config.num_sample_channels - 1)}});
  auto init_fsamples = InitFSamples(
    ctx,
    rt,
    config,
    gen.index_space(),
    receivers_by_shard,
    config.init_receiver_block_size,
    config.init_rng_seed,
    Mapper::level0_sharding_tag);
  auto send_fsamples = SendFSamples(
    ctx,
    rt,
    config,
    shard_point,
    receivers_by_shard,
    gen.index_space(),
    Mapper::level0_sharding_tag);

  auto init_distributions = InitSampleDistributions(ctx, rt, config);
  auto inverse_cdf = init_distributions.new_inverse_cdf_region(ctx, rt);
  init_distributions.launch(ctx, rt, inverse_cdf);
  auto ts = GetEpoch().launch(ctx, rt).get<std::chrono::nanoseconds>() + 2s;
  bool exit = false;
  while (!exit) {
    // auto [t0, fsamples] = fsend_generator.next(ctx, rt);
    auto fsamples = gen.make(ctx, rt);
    init_fsamples.launch(ctx, rt, fsamples, inverse_cdf);
    send_fsamples.launch(ctx, rt, fsamples, ts);
    ts += config.send_interval();
    rt->destroy_logical_region(ctx, fsamples);
  }
  rt->destroy_logical_region(ctx, inverse_cdf);
  gen.destroy(ctx, rt);
  send_fsamples.destroy(ctx, rt);
  init_distributions.destroy(ctx, rt);
}
} // namespace rcp::fsim

template <>
auto
rcp::get_log_sink<rcp::fsim::Tag>() -> LogSink<rcp::fsim::Tag>& {
  return rcp::fsim::log_fsim;
}

auto
main(int argc, char* argv[]) -> int {

  namespace L = Legion;

  L::Runtime::initialize(&argc, &argv);

  rcp::fsim::GetEpoch::preregister();
  rcp::fsim::GetConfig::preregister();
  rcp::fsim::InitFSamples ::preregister(rcp::fsim::init_fsamples_task_id);
  rcp::fsim::SendFSamples ::preregister(rcp::fsim::send_fsamples_task_id);
  rcp::fsim::InitSampleDistributions ::preregister(
    rcp::fsim::init_sample_distributions_task_id);

  L::TaskVariantRegistrar registrar(rcp::fsim::fsim_task_id, "fsim");
  registrar.add_constraint(L::ProcessorConstraint(L::Processor::LOC_PROC));
  registrar.set_replicable();
  L::Runtime ::preregister_task_variant<rcp::fsim::fsim_task>(
    registrar, "fsim task");
  L::Runtime::set_top_level_task_id(rcp::fsim::fsim_task_id);
  rcp::Mapper::preregister();

  return L::Runtime::start(argc, argv);
}

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
