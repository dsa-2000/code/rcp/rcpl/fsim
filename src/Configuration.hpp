// Copyright 2022 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include <rcp/DataPacketConfiguration.hpp>

#include <array>
#include <fstream>
#include <ostream>
#include <string>

namespace rcp::fsim {

struct Configuration {

  /*! \brief rng value type */
  using gen_type = unsigned short;

  bool show{};
  bool dry_run{};
  std::string error;
  std::string help;
  std::string input_file;

  /*! maximum number of streams per data capture instance
   *
   * this value is used in the specialization of rc::dc::Message used by
   * DataCapture, and so is part of the F-Engine/radio camera interface
   */
  static constexpr auto max_streams_per_dc = RCP_MAX_STREAMS_PER_DATA_CAPTURE;

  static constexpr auto num_sample_part_values =
    (1 << (4 * sizeof(DataPacketConfiguration::fsample_type)));

  /*! \brief total number of sample channels*/
  unsigned num_sample_channels{};
  /*! \brief maximum number of FEngine instances per shard */
  unsigned max_num_fe_per_shard{};
  /*! \brief size of memory reserved for packet capture, in number of packets */
  std::size_t packet_block_size{};
  /*! \brief size of (libfabric) packet capture completion queue */
  std::size_t completion_queue_size{};
  /*! \brief libfabric provider name */
  std::string fabric_provider;
  /*! \brief multicast group address for either capture parameter broadcasts or
   * packet destinations */
  std::string multicast_address;
  /*! \brief flag to indicate whether packets are sent to multicast group
   * addresses
   *
   * When this flag is asserted, the application manages the lifetime of the
   * multicast groups to which its packets are sent/addressed.
   *
   * The alternative is for packet receivers to send their channel-mapped
   * destination addresses to a multicast address that is subscribed by this
   * application. The alternative method may be useful for RoCE or IB, not plain
   * Ethernet.
   */
  bool multicast_packets{true};
  /*! \brief minimum time span of data send regions (ms) */
  unsigned min_send_interval_ms{};
  /*! \brief receivers partition size for sample initialization */
  unsigned init_receiver_block_size{};
  /*! \brief rng seed for sample initialization */
  int init_rng_seed{};
  /*! \brief probability mass function for real part of samples */
  std::array<float, num_sample_part_values> real_part_pmf;
  /*! \brief probability mass function for imaginary part of samples */
  std::array<float, num_sample_part_values> imag_part_pmf;
  /*! \brief seconds between progress reports */
  unsigned progress_report_interval_sec{};
  /*! \brief initial events file name
   *
   * Not used, but needed by rcp::GetConfig */
  std::string initial_events_file{};
  /*! \brief initial events script
   *
   * Also not used, but needed by rcp::GetConfig */
  std::string initial_events_script{};

  Configuration() = default;
  Configuration(int argc, char* const* argv, bool no_write = false);
  Configuration(Configuration const&) = default;
  Configuration(Configuration&&) = default;
  auto
  operator=(Configuration const&) -> Configuration& = default;
  auto
  operator=(Configuration&&) -> Configuration& = default;

  /*! time span of data send regions */
  [[nodiscard]] auto
  send_interval() const -> std::chrono::nanoseconds;

  /*! number of timesteps in a send region */
  [[nodiscard]] auto
  num_send_timesteps() const -> std::uint64_t;

  [[nodiscard]] auto
  progress_report_interval() const -> std::chrono::seconds;

  [[nodiscard]] auto
  do_show() const -> bool;

  [[nodiscard]] auto
  do_run() const -> bool;

  /*! write configuration to file stream */
  auto
  write_config(std::ofstream& of) -> void;

  [[nodiscard]] auto
  serialized_size() const -> std::size_t;

  auto
  serialize(void* buffer) const -> std::size_t;

  auto
  deserialize(void const* buffer) -> std::size_t;

  auto
  normalize_pmfs() -> void;
};

class ConfigurationSerdez {
public:
  using FIELD_TYPE = Configuration;
  static std::size_t const MAX_SERIALIZED_SIZE = 8192;

  static auto
  serialized_size(FIELD_TYPE const& val) -> std::size_t;

  static auto
  serialize(FIELD_TYPE const& val, void* buffer) -> std::size_t;

  static auto
  deserialize(FIELD_TYPE& val, void const* buffer) -> std::size_t;

  static void
  destroy(FIELD_TYPE& val);
};

auto
operator<<(std::ostream&, Configuration const&) -> std::ostream&;

} // namespace rcp::fsim

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
