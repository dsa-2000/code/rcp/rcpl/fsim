// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "Configuration.hpp"

#include <cctype>
#include <cmath>
#include <filesystem>
#include <numeric>
#include <regex>

#include <boost/program_options.hpp>
#include <ios>

namespace {

namespace po = boost::program_options;

template <typename T>
struct Show {
  T t;
  auto
  insert_to(std::ostream& os) const -> std::ostream& {
    return os << t;
  }
};

template <>
struct Show<bool> {
  bool t;
  auto
  insert_to(std::ostream& os) const -> std::ostream& {
    return os << std::boolalpha << t << std::noboolalpha;
  }
};

template <typename T>
struct Show<std::vector<T>> {
  std::vector<T> tvect;
  auto
  insert_to(std::ostream& os) const -> std::ostream& {
    char const* sep = "";
    for (auto&& tval : tvect) {
      os << sep;
      Show<T>{tval}.insert_to(os);
      sep = " ";
    }
    return os;
  }
};

template <typename T, std::size_t N>
struct Show<std::array<T, N>> {
  std::array<T, N> ary;
  auto
  insert_to(std::ostream& os) const -> std::ostream& {
    char const* sep = "";
    for (auto&& tval : ary) {
      os << sep;
      Show<T>{tval}.insert_to(os);
      sep = " ";
    }
    return os;
  }
};

template <typename T>
Show(T&&) -> Show<std::remove_cvref_t<T>>;

auto
operator<<(std::ostream& os, rcp::Insertable auto insertable) -> std::ostream& {
  return insertable.insert_to(os);
}

template <typename... Params>
auto
write_config(
  std::ofstream& of,
  std::string const& section,
  po::options_description const& opts,
  Params&&... params) -> void {

  auto par = [&opts](auto&& nm_val) -> std::string {
    auto const& [nm, val] = nm_val;
    auto short_nm = std::string_view(&nm[nm.rfind('.') + 1]);
    std::ostringstream oss;
    oss << "# " << opts.find_nothrow(nm, false)->description() << '\n'
        << short_nm << '=' << Show(val) << '\n';
    return oss.str();
  };
  of << '[' << section << "]\n";
  (void)(of << ... << par(std::forward<Params>(params)));
}

// configuration helper functions
//
auto
fsim_config_var(std::string_view ev) -> std::string {

  using namespace std::string_literals;

  std::string result;
  static std::string const fsim = "FSIM_";
  if (ev.starts_with(fsim)) {
    std::ranges::transform(
      std::string_view(fsim.c_str(), fsim.size() - 1),
      std::back_inserter(result),
      [](auto&& ch) { return std::tolower(ch); });
    result += ".";
    ev = ev.substr(fsim.size());

    std::ranges::transform(
      ev, std::back_inserter(result), [](auto&& ch) -> char {
        if (ch == '_')
          return '-';
        return std::tolower(ch);
      });
  }
  return result;
}

auto
input_cfgfile_var(std::string_view ev) -> std::string {
  std::string result;
  if (ev == "FSIM_CONFIG_FILE")
    result = "fsim.config-file";
  return result;
}

auto
usage() -> std::string {
  using namespace std::string_literals;
  static std::string const result =
    "fsim: DSA-2000 prototype F-engine simulator application\n"s
    + "  (Contact: Martin Pokorny <mpokorny@caltech.edu>)\n\n"s
    + "Every command line option below corresponds with an environment\n"s
    + "variable named according to the pattern fsim.wx.yz.var-iable -> \n"s
    + "FSIM_WX_YZ_VAR_IABLE (e.g, fsim.num-sample-channels -> \n"s
    + "FSIM_NUM_SAMPLE_CHANNELS)\n\n"s + "Options";
  return result;
}

} // namespace

namespace rcp::fsim {

namespace po = boost::program_options;

struct PMF {
  decltype(Configuration::real_part_pmf) pmf;
};

auto
validate(
  boost::any& anyv,
  std::vector<std::string> const& values,
  PMF* /*name*/,
  int /*nada*/) -> void {

  // regex for whitespace-separated words
  static std::regex word_regex("(?:\\s*(\\S+))");

  // Make sure no previous assignment to 'a' was made.
  po::validators::check_first_occurrence(anyv);

  decltype(Configuration::real_part_pmf) pmf;
  auto pmf_element = pmf.begin();
  for (auto&& val : values) {
    if (std::distance(&values.front(), &val) >= pmf.size())
      throw boost::program_options::error("pmf length too long");
    if (!val.empty()) {
      std::string str = val;
      std::smatch words;
      while (std::regex_search(str, words, word_regex)) {
        *pmf_element = std::atof(words[1].str().c_str());
        if (*pmf_element < 0)
          throw boost::program_options::error("negative pmf value");
        ++pmf_element;
        str = words.suffix();
      }
    }
  }
  if (std::distance(pmf.begin(), pmf_element) != pmf.size())
    throw boost::program_options::error("pmf length too short");
  anyv = PMF{pmf};
}

auto
options(Configuration& config) -> po::options_description {
  po::options_description result("top-level options");
  std::ostringstream real_pmf_desc;
  real_pmf_desc << Show{config.real_part_pmf};
  std::ostringstream imag_pmf_desc;
  imag_pmf_desc << Show{config.imag_part_pmf};
  PMF* real_pmf = reinterpret_cast<PMF*>(&config.real_part_pmf);
  PMF* imag_pmf = reinterpret_cast<PMF*>(&config.imag_part_pmf);
  result.add_options()(
    "fsim.num-sample-channels",
    po::value(&config.num_sample_channels)
      ->default_value(config.num_sample_channels),
    "total number of sample channels")(
    "fsim.max-num-fe-instances-per-shard",
    po::value(&config.max_num_fe_per_shard)
      ->default_value(config.max_num_fe_per_shard),
    "maximum number of fe instances per shard")(
    "fsim.min-send-interval",
    po::value(&config.min_send_interval_ms)
      ->default_value(config.min_send_interval_ms),
    "minimum send interval (milliseconds)")(
    "fsim.packet-block-size",
    po::value(&config.packet_block_size)
      ->default_value(config.packet_block_size),
    "packet memory block size (number of packets)")(
    "fsim.completion-queue-size",
    po::value(&config.completion_queue_size)
      ->default_value(config.completion_queue_size),
    "completion queue size (number of packets)")(
    "fsim.fabric-provider",
    po::value(&config.fabric_provider)->default_value(config.fabric_provider),
    "fabric provider")(
    "fsim.multicast-address",
    po::value(&config.multicast_address)
      ->default_value(config.multicast_address),
    "multicast address")(
    "fsim.multicast-packets",
    po::value(&config.multicast_packets)
      ->default_value(config.multicast_packets),
    "packet multicast flag")(
    "fsim.init-receiver-block-size",
    po::value(&config.init_receiver_block_size)
      ->default_value(config.init_receiver_block_size),
    "receivers partition size for sample initialization")(
    "fsim.init-rng-seed",
    po::value(&config.init_rng_seed)->default_value(config.init_rng_seed),
    "rng seed for sample initialization")(
    "fsim.real-part-pmf",
    po::value(real_pmf)->default_value(*real_pmf, real_pmf_desc.str()),
    "probability mass function for real part of samples")(
    "fsim.imag-part-pmf",
    po::value(imag_pmf)->default_value(*imag_pmf, imag_pmf_desc.str()),
    "probability mass function for imaginary part of samples")(
    "fsim.progress-report-interval",
    po::value(&config.progress_report_interval_sec)
      ->default_value(config.progress_report_interval_sec),
    "interval between progress reports (sec)");
  return result;
}

// Configuration member functions
//
Configuration::Configuration(int argc, char* const* argv, bool no_write)
  : num_sample_channels(16)
  , max_num_fe_per_shard(1)
  , packet_block_size(100'000)
  , completion_queue_size(50'000)
  , fabric_provider("udp")
  , multicast_address("239.255.1.100:12000")
  , min_send_interval_ms(10)
  , init_receiver_block_size(10)
  , init_rng_seed(0)
  , progress_report_interval_sec(5) {

  std::ranges::fill(real_part_pmf, 1.0f);
  std::ranges::fill(imag_part_pmf, 1.0f);

  po::options_description generic("generic options");
  generic.add_options()("help", "produce help message")(
    "dry-run,n", po::bool_switch(&dry_run), "show configuration, then exit")(
    "show", po::bool_switch(&show), "show configuration");

  bool update_config = false;
  std::string config_out;
  po::options_description input("input options");
  input.add_options()(
    "fsim.config-file,c", po::value(&input_file), "configuration file");
  if (!no_write)
    input.add_options()(
      "update-config-file,u",
      po::bool_switch(&update_config),
      "update configuration file")(
      "write-config-file,w", po::value(&config_out), "new configuration file");

  // handle input options
  try {
    po::variables_map input_vm;
    po::store(
      po::command_line_parser(argc, argv)
        .options(input)
        .allow_unregistered()
        .run(),
      input_vm);
    po::store(po::parse_environment(input, input_cfgfile_var), input_vm);
    po::notify(input_vm);
  } catch (boost::program_options::error const&) {
  }

  // first scan to determine gridding and image names
  po::options_description setup("setup options");
  try {
    po::variables_map setup_vm;
    po::store(
      po::command_line_parser(argc, argv)
        .options(setup)
        .allow_unregistered()
        .run(),
      setup_vm);
    po::store(po::parse_environment(setup, fsim_config_var), setup_vm);
    if (input_file.size() > 0)
      po::store(
        po::parse_config_file(input_file.c_str(), setup, true), setup_vm);
    po::notify(setup_vm);
  } catch (boost::program_options::error const&) {
  }

  auto config = options(*this);

  po::options_description desc(usage());
  desc.add(generic).add(input).add(config);

  // now do option parsing
  po::variables_map vm;
  try {
    po::store(
      po::command_line_parser(argc, argv)
        .options(desc)
        .allow_unregistered()
        .run(),
      vm);
    po::store(po::parse_environment(desc, fsim_config_var), vm);
    if (input_file.size() > 0) {
      po::options_description cfgfile_options;
      cfgfile_options.add(config);
      po::store(po::parse_config_file(input_file.c_str(), cfgfile_options), vm);
    }
    po::notify(vm);
    if (vm.count("help") > 0) {
      std::ostringstream oss;
      oss << desc;
      help = oss.str();
    }
  } catch (boost::program_options::error const& e) {
    error = e.what();
  }

  num_sample_channels = rcp::round_up(
    num_sample_channels, DataPacketConfiguration::num_channels_per_packet);

  if (error.size() == 0 && help.size() == 0) {
    if (update_config && !input_file.empty() && config_out.empty()) {
      if (std::filesystem::exists(input_file))
        std::filesystem::copy_file(input_file, input_file + ".bak");
      std::ofstream f(input_file, std::ios::trunc | std::ios::out);
      write_config(f);
    } else if (!config_out.empty()) {
      if (std::filesystem::exists(config_out))
        std::filesystem::copy_file(config_out, config_out + ".bak");
      std::ofstream f(config_out, std::ios::trunc | std::ios::out);
      write_config(f);
    }
  }
}

auto
Configuration::send_interval() const -> std::chrono::nanoseconds {
  return std::chrono::duration_cast<std::chrono::nanoseconds>(
    num_send_timesteps() * DataPacketConfiguration::fsample_interval);
}

auto
Configuration::progress_report_interval() const -> std::chrono::seconds {
  return std::chrono::seconds{progress_report_interval_sec};
}

auto
Configuration::num_send_timesteps() const -> std::uint64_t {

  DataPacketConfiguration::fsample_duration_t packet_interval{
    DataPacketConfiguration::num_timesteps_per_packet};
  return rcp::ceil_div(
           std::uint64_t(std::chrono::nanoseconds(
                           std::chrono::milliseconds{min_send_interval_ms})
                           .count()),
           std::uint64_t(std::chrono::duration_cast<std::chrono::nanoseconds>(
                           packet_interval)
                           .count()))
         * DataPacketConfiguration::num_timesteps_per_packet;
}

auto
Configuration::do_show() const -> bool {
  return error.size() > 0 || help.size() > 0 || show || dry_run;
}

auto
Configuration::do_run() const -> bool {
  return !(dry_run || error.size() > 0 || help.size() > 0);
}

void
Configuration::write_config(std::ofstream& of) {
  using namespace std::string_literals;
  of << "##\n## fsim configuration file\n##\n\n";
  ::write_config(
    of,
    "fsim",
    options(*this),
    std::make_tuple("fsim.num-sample-channels"s, num_sample_channels),
    std::make_tuple(
      "fsim.max-num-fe-instances-per-shard"s, max_num_fe_per_shard),
    std::make_tuple("fsim.packet-block-size"s, packet_block_size),
    std::make_tuple("fsim.completion-queue-size"s, completion_queue_size),
    std::make_tuple("fsim.fabric-provider"s, fabric_provider),
    std::make_tuple("fsim.multicast-address"s, multicast_address),
    std::make_tuple("fsim.multicast-packets"s, multicast_packets),
    std::make_tuple("fsim.min-send-interval"s, min_send_interval_ms),
    std::make_tuple("fsim.init-receiver-block-size"s, init_receiver_block_size),
    std::make_tuple("fsim.init-rng-seed"s, init_rng_seed),
    std::make_tuple("fsim.real-part-pmf"s, real_part_pmf),
    std::make_tuple("fsim.imag-part-pmf"s, imag_part_pmf),
    std::make_tuple(
      "fsim.progress-report-interval"s, progress_report_interval_sec));
}

auto
Configuration::serialized_size() const -> std::size_t {

  std::size_t result =
    rcp::serialized_size(show) + rcp::serialized_size(dry_run)
    + rcp::serialized_size(error) + rcp::serialized_size(help)
    + rcp::serialized_size(input_file)
    + rcp::serialized_size(num_sample_channels)
    + rcp::serialized_size(max_num_fe_per_shard)
    + rcp::serialized_size(packet_block_size)
    + rcp::serialized_size(completion_queue_size)
    + rcp::serialized_size(fabric_provider)
    + rcp::serialized_size(multicast_address)
    + rcp::serialized_size(multicast_packets)
    + rcp::serialized_size(min_send_interval_ms)
    + rcp::serialized_size(init_receiver_block_size)
    + rcp::serialized_size(init_rng_seed) + rcp::serialized_size(real_part_pmf)
    + rcp::serialized_size(imag_part_pmf)
    + rcp::serialized_size(progress_report_interval_sec);
  return result;
}

auto
Configuration::serialize(void* buffer) const -> std::size_t {
  char* b = reinterpret_cast<char*>(buffer);
  b += rcp::serialize(show, b);
  b += rcp::serialize(dry_run, b);
  b += rcp::serialize(error, b);
  b += rcp::serialize(help, b);
  b += rcp::serialize(input_file, b);
  b += rcp::serialize(num_sample_channels, b);
  b += rcp::serialize(max_num_fe_per_shard, b);
  b += rcp::serialize(packet_block_size, b);
  b += rcp::serialize(completion_queue_size, b);
  b += rcp::serialize(fabric_provider, b);
  b += rcp::serialize(multicast_address, b);
  b += rcp::serialize(multicast_packets, b);
  b += rcp::serialize(min_send_interval_ms, b);
  b += rcp::serialize(init_receiver_block_size, b);
  b += rcp::serialize(init_rng_seed, b);
  b += rcp::serialize(real_part_pmf, b);
  b += rcp::serialize(imag_part_pmf, b);
  b += rcp::serialize(progress_report_interval_sec, b);
  return b - reinterpret_cast<char*>(buffer);
}

auto
Configuration::deserialize(void const* buffer) -> std::size_t {
  char const* b = reinterpret_cast<char const*>(buffer);
  b += rcp::deserialize(show, b);
  b += rcp::deserialize(dry_run, b);
  b += rcp::deserialize(error, b);
  b += rcp::deserialize(help, b);
  b += rcp::deserialize(input_file, b);
  b += rcp::deserialize(num_sample_channels, b);
  b += rcp::deserialize(max_num_fe_per_shard, b);
  b += rcp::deserialize(packet_block_size, b);
  b += rcp::deserialize(completion_queue_size, b);
  b += rcp::deserialize(fabric_provider, b);
  b += rcp::deserialize(multicast_address, b);
  b += rcp::deserialize(multicast_packets, b);
  b += rcp::deserialize(min_send_interval_ms, b);
  b += rcp::deserialize(init_receiver_block_size, b);
  b += rcp::deserialize(init_rng_seed, b);
  b += rcp::deserialize(real_part_pmf, b);
  b += rcp::deserialize(imag_part_pmf, b);
  b += rcp::deserialize(progress_report_interval_sec, b);
  return b - reinterpret_cast<char const*>(buffer);
}

auto
Configuration::normalize_pmfs() -> void {
  for (auto&& pmf : {&real_part_pmf, &imag_part_pmf}) {
    auto sum = std::accumulate(pmf->begin(), pmf->end(), 0.0F, std::plus<>{});
    for (auto&& prob : *pmf)
      prob /= sum;
  }
}

// Configuration stream insertion
//
auto
operator<<(std::ostream& os, Configuration const& config) -> std::ostream& {

  auto indent =
    [](std::string const& str, std::string const& tab) -> std::string {
    std::string result;
    result.reserve(str.size() + tab.size());
    result.append(tab);
    for (auto&& ch : std::string_view(str.c_str(), str.size() - 1))
      if (ch != '\n')
        result.push_back(ch);
      else
        result.append(ch + tab);
    result.push_back(str.back());
    return result;
  };

  if (config.help.size() > 0) {
    os << config.help;
  } else if (config.error.size() > 0) {
    os << config.error;
  } else {
    std::string dp_component =
      "Data packet:\n" + indent(rcp::show(DataPacketConfiguration()), "  ");
    os << "Configuration:" << '\n'
       << indent(dp_component, "  ")
       << "  num_sample_channels: " << config.num_sample_channels << '\n'
       << "  max_num_fe_per_shard: " << config.max_num_fe_per_shard << '\n'
       << "  packet_block_size: " << config.packet_block_size << '\n'
       << "  completion_queue_size: " << config.completion_queue_size << '\n'
       << "  fabric_provider: " << config.fabric_provider << '\n'
       << "  multicast_address: " << config.multicast_address << '\n'
       << "  multicast_packets: " << std::boolalpha << config.multicast_packets
       << std::noboolalpha << '\n'
       << "  send_interval [dynamic]: "
       << std::chrono::duration_cast<std::chrono::nanoseconds>(
            config.send_interval())
            .count()
       << " ns\n"
       << "  num_send_timesteps [dynamic]: " << config.num_send_timesteps()
       << '\n'
       << "  init_receiver_block_size: " << config.init_receiver_block_size
       << '\n'
       << "  init_rng_seed: " << config.init_rng_seed << '\n'
       << "  real_part_pmf: " << Show(config.real_part_pmf) << '\n'
       << "  imag_part_pmf: " << Show(config.imag_part_pmf) << '\n'
       << "  progress_report_interval: "
       << std::chrono::duration_cast<std::chrono::seconds>(
            config.progress_report_interval())
            .count()
       << " s\n";
  }
  return os;
}
} // end namespace rcp::fsim

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
