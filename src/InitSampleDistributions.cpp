// Copyright 2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "InitSampleDistributions.hpp"
#include "CDFInverseRegion.hpp"

#include <algorithm>
#include <limits>
#include <ranges>
#include <rcp/rcp.hpp>
#include <type_traits>
#include <vector>

namespace rcp::fsim {

/*! \brief initialize a CDF from a PMF
 *
 * The CDF values are the (excluded) minimum value of RNG values in a bin. That
 * is, a value v falls into the CDF array bin at b iff CDF[b] < v <= CDF[b+1]
 * (without the upper bound when b is Configuration::num_sample_part_values -
 * 1). Values of CDF are scaled so that probability 0 corresponds to a CDF value
 * 0, and probability 1, to a CDF value of std::numeric_limits<T>::max().
 */
template <typename T>
  requires std::is_unsigned_v<T>
auto
init_cdf(
  std::array<T, Configuration::num_sample_part_values>& cdf,
  std::array<float, Configuration::num_sample_part_values> const& pmf) {

  auto constexpr max_t = std::numeric_limits<T>::max();
  auto floor = 0.0;
  for (auto&& idx : std::views::iota(std::size_t{0}, pmf.size())) {
    assert(floor <= 1.0);
    cdf[idx] = T(floor * max_t + 0.5);
    floor += pmf[idx];
  }
}

/*! \brief compute the inverse of a CDF mapping at some value
 *
 * \param tval scaled probability value in [0, std::numeric_limits<T>::max()]
 *
 * \return sample value s.t. CDF[sample value] == probability value
 *
 * By uniformly randomly sampling the range of T, applying this function to
 * those values creates a distribution of values with the given CDF.
 */
template <typename T>
auto
invert_cdf(
  std::array<T, Configuration::num_sample_part_values> const& cdf,
  T tval) -> int {

  return (std::distance(
            std::lower_bound(cdf.begin() + 1, cdf.end(), tval), cdf.begin())
          - 1)
         * (static_cast<int>(Configuration::num_sample_part_values) / 2);
}

/*! \brief implementation of task to initialize inverse CDF region values */
struct InitSampleDistributionsTask
  : public SerialOnlyTaskMixin<InitSampleDistributionsTask, void> {

  /* \brief task name */
  static constexpr char const* task_name = "InitSampleDistributions";

  /*! \brief task id */
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  /*! \brief region indexes */
  enum { cdf_inverse_region_index, num_regions };

  /*! \brief RNG value type alias */
  using gen_type = Configuration::gen_type;

  /*! \brief CDFInverseRegion type alias */
  using icdf_t = CDFInverseRegion;

  /*! \brief task body */
  static auto
  serial_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> result_t {

    assert(regions.size() == num_regions);
    assert(task->arglen == sizeof(InitSampleDistributions::Args));
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    auto const& args =
      *reinterpret_cast<InitSampleDistributions::Args const*>(task->args);
    auto const& real_pmf = args.real_pmf;
    auto const& imag_pmf = args.imag_pmf;

    std::array<Configuration::gen_type, Configuration::num_sample_part_values>
      real_cdf;
    std::array<Configuration::gen_type, Configuration::num_sample_part_values>
      imag_cdf;
    init_cdf(real_cdf, real_pmf);
    init_cdf(imag_cdf, imag_pmf);

    auto const& cdf_inverse_region = regions[cdf_inverse_region_index];
    auto const real_cdf_inverse = icdf_t::values<LEGION_WRITE_DISCARD>(
      cdf_inverse_region, CDFRealPartField{});
    auto const imag_cdf_inverse = icdf_t::values<LEGION_WRITE_DISCARD>(
      cdf_inverse_region, CDFImagPartField{});
    for (auto pir = L::PointInRectIterator<icdf_t::dim, icdf_t::coord_t>(
           cdf_inverse_region);
         pir();
         pir++) {
      real_cdf_inverse[*pir] =
        invert_cdf(real_cdf, static_cast<Configuration::gen_type>(pir[0]));
      imag_cdf_inverse[*pir] =
        invert_cdf(imag_cdf, static_cast<Configuration::gen_type>(pir[0]));
    }
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
Legion::TaskID InitSampleDistributionsTask::task_id;

InitSampleDistributions::InitSampleDistributions(
  L::Context ctx, L::Runtime* rt, Configuration const& config) {

  m_gen = DenseArrayRegionGenerator<icdf_t>(ctx, rt, {});
  std::ranges::copy(config.real_part_pmf, m_args.real_pmf.begin());
  std::ranges::copy(config.imag_part_pmf, m_args.imag_pmf.begin());
}

auto
InitSampleDistributions::launch(
  L::Context ctx, L::Runtime* rt, icdf_t::LogicalRegion inverse_cdf) -> void {

  auto launcher = L::TaskLauncher(
    InitSampleDistributionsTask::task_id,
    L::UntypedBuffer(&m_args, sizeof(m_args)));

  launcher.region_requirements.resize(InitSampleDistributionsTask::num_regions);
  launcher.region_requirements
    [InitSampleDistributionsTask::cdf_inverse_region_index] =
    inverse_cdf.requirement(
      LEGION_WRITE_DISCARD,
      LEGION_EXCLUSIVE,
      StaticFields{CDFRealPartField{}, CDFImagPartField{}});
  rt->execute_task(ctx, launcher);
}

auto
InitSampleDistributions::new_inverse_cdf_region(L::Context ctx, L::Runtime* rt)
  -> icdf_t::LogicalRegion {
  return m_gen.make(ctx, rt);
}

auto
InitSampleDistributions::destroy(L::Context ctx, L::Runtime* rt) -> void {
  m_gen.destroy(ctx, rt);
}

auto
InitSampleDistributions::preregister(L::TaskID tid) -> void {
  PortableTask<InitSampleDistributionsTask>::preregister_task_variants(tid);
}

} // namespace rcp::fsim

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
