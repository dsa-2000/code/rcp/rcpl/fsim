// Copyright 2023,2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "InitFSamples.hpp"
#include "CDFInverseRegion.hpp"
#include <rcp/rcp.hpp>

#ifdef RCP_USE_KOKKOS
#include <Kokkos_Random.hpp>
#endif // RCP_USE_KOKKOS

#include <functional>
#include <limits>
#include <ranges>
#include <vector>

namespace rcp::fsim {

/*! \brief task implementation to initialize F-engine sample values */
struct InitFSamplesTask
  : public DefaultKokkosTaskMixin<InitFSamplesTask, void> {

  /*! \brief task name */
  static constexpr char const* task_name = "InitFSamples";

  /*! \brief task id */
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  /*! \brief task region indexes*/
  enum { samples_region_index, cdf_inverse_region_index, num_regions };

  /*! \brief FSamplesRegion type alias */
  using md_t = FSamplesRegion;

  /*! \brief RNG data type alias */
  using gen_type = Configuration::gen_type;

  /*! \brief CDFInverseRegion type alias */
  using icdf_t = CDFInverseRegion;

  /*! \brief number of bits per real or imaginary part of a sample value */
  static constexpr auto nbits_per_part = 4 * sizeof(FSampleField::kvalue_t);

#ifdef RCP_USE_KOKKOS
  /*! \brief sample initialization,  Kokkos implementation */
  template <typename execution_space>
  static auto
  kokkos_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> result_t {

    assert(regions.size() == num_regions);
    assert(task->arglen == sizeof(InitFSamples::Args));

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    auto seed = reinterpret_cast<InitFSamples::Args const*>(task->args)->seed;
    seed ^= InitFSamples::seed_t(task->index_point[0]);

    auto const& samples_region = regions[samples_region_index];
    md_t::rect_t bounds = samples_region;
    auto const samples = md_t::view<execution_space, LEGION_WRITE_DISCARD>(
      samples_region, FSampleField{});

    auto const& cdf_inverse_region = regions[cdf_inverse_region_index];
    auto const real_cdf_inverse =
      icdf_t::view<execution_space, LEGION_READ_ONLY>(
        cdf_inverse_region, CDFRealPartField{});
    auto const imag_cdf_inverse =
      icdf_t::view<execution_space, LEGION_READ_ONLY>(
        cdf_inverse_region, CDFImagPartField{});

    auto work_space = rt->get_executing_processor(ctx).kokkos_work_space();
    assert(
      rt->get_executing_processor(ctx).kind()
      == KokkosProcessor<execution_space>::kind);

    K::Random_XorShift64_Pool<execution_space> random_pool(seed);
    K::parallel_for(
      task_name,
      mdrange_policy<
        execution_space,
        md_t,
        md_t::axes_t::receiver,
        md_t::axes_t::channel,
        md_t::axes_t::polarization,
        md_t::axes_t::timestep>(work_space, bounds),
      KOKKOS_LAMBDA(coord_t rcv, coord_t ch, coord_t pol, coord_t ts) {
        auto gen = random_pool.get_state();
        static_assert(sizeof(decltype(gen.urand())) >= sizeof(gen_type));
        auto real =
          real_cdf_inverse[gen.urand() & std::numeric_limits<gen_type>::max()];
        auto imag =
          imag_cdf_inverse[gen.urand() & std::numeric_limits<gen_type>::max()];
        samples(ch, rcv, pol, ts).i = std::int8_t(
          (imag << nbits_per_part) | real & ((1 << nbits_per_part) - 1));
        random_pool.free_state(gen);
      });
  }
#else  // !RCP_USE_KOKKOS
  /*! \brief sample initialization, serial implementation */
  static auto
  serial_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> result_t {

    assert(task->arglen == sizeof(InitFSamples::seed_t));
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    auto seed = *reinterpret_cast<InitFSamples::seed_t const*>(task->args);
    seed ^= InitFSamples::seed_t(task->index_point[0]);

    assert(regions.size() == 1);
    auto const& samples_region = regions[samples_region_index];
    L::Rect<md_t::dim, coord_t> bounds = samples_region;
    auto const samples =
      md_t::values<LEGION_WRITE_DISCARD>(samples_region, FSampleField{});

    auto const& cdf_inverse_region = regions[cdf_inverse_region_index];
    auto const real_cdf_inverse =
      icdf_t::values<LEGION_READ_ONLY>(cdf_inverse_region, CDFRealPartField{});
    auto const imag_cdf_inverse =
      icdf_t::values<LEGION_READ_ONLY>(cdf_inverse_region, CDFImagPartField{});

    constexpr auto nbits_per_byte = 8;
    std::mt19937 gen(seed);
    std::uniform_int_distribution<gen_type> distrib(
      std::numeric_limits<gen_type>::min(),
      std::numeric_limits<gen_type>::max());

    for (auto&& rcv : range(bounds, md_t::axes_t::receiver))
      for (auto&& ch : range(bounds, md_t::axes_t::channel))
        for (auto&& pol : range(bounds, md_t::axes_t::polarization))
          for (auto&& ts : range(bounds, md_t::axes_t::timestep)) {
            coord_t pt[]{ch, rcv, pol, ts};
            auto real = real_cdf_inverse[distrib(gen)];
            auto imag = imag_cdf_inverse[distrib(gen)];
            samples[L::Point<4, coord_t>(pt)].i = std::int8_t(
              (imag << nbits_per_part) | real & ((1 << nbits_per_part) - 1));
          }
  }
#endif // RCP_USE_KOKKOS

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    using traits = task_variant_traits<Variant>;
    registrar.set_leaf();
    registrar.add_layout_constraint_set(
      InitFSamplesTask::samples_region_index,
      samples_layout().constraint_id(traits::layout_style).value());
    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
Legion::TaskID InitFSamplesTask::task_id;

InitFSamples::InitFSamples(
  L::Context ctx,
  L::Runtime* rt,
  Configuration const& config,
  md_t::index_space_t index_space,
  receivers_def_t const& receivers_by_shard,
  unsigned receiver_block_size,
  seed_t init_rng_seed,
  L::MappingTagID tag)
  : m_gen(init_rng_seed)
  , m_distrib(
      std::numeric_limits<seed_t>::min(), std::numeric_limits<seed_t>::max()) {

  // create two-level partition: by shard and by receiver block
  {
    auto bounds = rt->get_index_space_domain(index_space).bounds;
    std::vector<L::Rect<2, coord_t>> cs_rects;
    std::map<L::Point<2, coord_t>, L::DomainT<md_t::dim, coord_t>> domains;
    for (auto&& rcv :
         std::views::iota(coord_t{0}, coord_t(receivers_by_shard.size()))) {
      auto nblk = rcp::ceil_div(
        unsigned{receivers_by_shard[rcv][1]}, receiver_block_size);
      cs_rects.push_back(L::Rect<2, coord_t>{{rcv, 0}, {rcv, nblk - 1}});
      auto nfull = unsigned{receivers_by_shard[rcv][1]} / receiver_block_size;
      for (auto&& blk : std::views::iota(0U, nfull)) {
        bounds.lo[dim(md_t::axes_t::receiver)] =
          receivers_by_shard[rcv][0] + blk * receiver_block_size;
        bounds.hi[dim(md_t::axes_t::receiver)] =
          bounds.lo[dim(md_t::axes_t::receiver)] + receiver_block_size - 1;
        domains[{rcv, blk}] = L::DomainT<md_t::dim, coord_t>(bounds);
      }
      if (nblk > nfull) {
        auto rem = unsigned{receivers_by_shard[rcv][1]} % receiver_block_size;
        assert(rem != 0);
        bounds.lo[dim(md_t::axes_t::receiver)] =
          receivers_by_shard[rcv][0] + nfull * receiver_block_size;
        bounds.hi[dim(md_t::axes_t::receiver)] =
          bounds.lo[dim(md_t::axes_t::receiver)] + rem - 1;
        domains[{rcv, nfull}] = L::DomainT<md_t::dim, coord_t>(bounds);
      }
    }
    auto cs = rt->create_index_space(ctx, cs_rects);
    m_ip = rt->create_partition_by_domain(
      ctx, index_space, domains, cs, true, LEGION_DISJOINT_COMPLETE_KIND);
  }

  m_launcher = L::IndexTaskLauncher(
    InitFSamplesTask::task_id,
    rt->get_index_partition_color_space(m_ip),
    L::UntypedBuffer(),
    L::ArgumentMap());
  m_launcher.tag = tag;
  m_launcher.region_requirements.resize(InitFSamplesTask::num_regions);
}

auto
InitFSamples::launch(
  L::Context ctx,
  L::Runtime* rt,
  md_t::LogicalRegion samples,
  icdf_t::LogicalRegion inverse_cdf) -> void {

  m_args.seed = m_distrib(m_gen);
  m_launcher.global_arg = L::UntypedBuffer(&m_args, sizeof(m_args));
  auto lp = md_t::LogicalPartition{rt->get_logical_partition(samples, m_ip)};
  m_launcher.region_requirements[InitFSamplesTask::samples_region_index] =
    lp.requirement(
      0,
      LEGION_WRITE_DISCARD,
      LEGION_EXCLUSIVE,
      samples,
      StaticFields{FSampleField{}});
  m_launcher.region_requirements[InitFSamplesTask::cdf_inverse_region_index] =
    inverse_cdf.requirement(
      LEGION_READ_ONLY,
      LEGION_EXCLUSIVE,
      StaticFields{CDFRealPartField{}, CDFImagPartField{}});
  rt->execute_index_space(ctx, m_launcher);
}

auto
InitFSamples::preregister(L::TaskID tid) -> void {
  PortableTask<InitFSamplesTask>::preregister_task_variants(tid);
}
} // namespace rcp::fsim

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
