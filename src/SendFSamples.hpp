// Copyright 2022-2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "Configuration.hpp"
#include "fsim.hpp"
#include <rcp/FSamplesRegion.hpp>
#include <rcp/dc/DataCaptureOFI.hpp>
#include <rcp/dc/rcpdc.hpp>

#include <chrono>
#include <future>
#include <map>
#include <memory>
#include <memory_resource>
#include <optional>
#include <queue>
#include <thread>
#include <vector>

#include <netinet/ip.h>

namespace rcp::fsim {

/*! \brief rc::dc::FPacket specialization for F-engine packets */
using fpacket_type = rcp::dc::FPacket<
  std::chrono::duration_cast<std::chrono::nanoseconds>(
    DataPacketConfiguration::fsample_interval)
    .count(),
  DataPacketConfiguration::num_channels_per_packet,
  DataPacketConfiguration::num_polarizations,
  DataPacketConfiguration::num_timesteps_per_packet,
  DataPacketConfiguration::timesteps_tile_size>;

/*! \brief wrapper for some useful OFI-related functions and values */
struct OFIUtil {

  /*! \brief OFI version */
  static int const fi_version;

  /*! \brief registered memory key */
  static std::uint64_t const mr_key;

  /*! \brief get fabric interfaces for named provider
   *
   * Only transmit/send attributes are used by this function
   */
  static auto
  find_fabric_interfaces(std::string const& provider)
    -> std::vector<oficpp::FiInfo>;

  /*! \brief check whether message prefix is needed on transmit/send */
  static inline auto
  requires_tx_message_prefix(oficpp::FiInfo const& info) -> bool {
    return (info->tx_attr->mode & FI_MSG_PREFIX) != 0;
  }

  /*! \brief size of message prefix for transmit/send */
  static inline auto
  tx_message_prefix_size(oficpp::FiInfo const& info) {
    return requires_tx_message_prefix(info) ? info->ep_attr->msg_prefix_size
                                            : 0;
  }

  /*! \brief total size of a message containing a packet for transmit/send */
  static auto
  tx_message_size(oficpp::FiInfo const& info) -> std::size_t {

    return tx_message_prefix_size(info) + sizeof(fpacket_type);
  }
};

/*! \brief aggregation of an endpoint name and resolved OFI address */
class EndpointAddr {
  /*! \brief endpoint name */
  rcp::dc::EndpointName m_name;
  /*! \brief resolved endpoint address */
  fi_addr_t m_addr;
  /*! \brief address vector maintaining the endpoint address */
  std::shared_ptr<oficpp::FidAv<>> m_av;

public:
  /*! \brief default constructor (deleted) */
  EndpointAddr() = delete;

  /*! \brief constructor from endpoint name and address vector
   *
   * Endpoint name is inserted into address vector upon construction
   */
  EndpointAddr(
    rcp::dc::EndpointName name, std::shared_ptr<oficpp::FidAv<>> const& av);

  /*! \brief copy constructor (deleted)
   *
   * Don't want to lose track of ownership of the address in the address vector,
   * so the addr value should not be copied.
   */
  EndpointAddr(EndpointAddr const&) = delete;

  /*! \brief move constructor */
  EndpointAddr(EndpointAddr&& other) noexcept;

  /*! \brief copy assignment (deleted)
   *
   * The addr value should not be copied.
   */
  auto
  operator=(EndpointAddr const&) -> EndpointAddr& = delete;

  /*! \brief move assignment */
  auto
  operator=(EndpointAddr&& rhs) -> EndpointAddr&;

  /*! \brief destructor */
  ~EndpointAddr();

  /*! \brief endpoint name */
  auto
  name() const -> rcp::dc::EndpointName const&;

  /*! \brief endpoint address */
  auto
  addr() const -> fi_addr_t const&;
};

/*! \brief base class for FEngine
 *
 * erases template value from FEngine<N>
 */
class FEngineBase {

protected:
  /*! \brief memory resource for packets */
  std::shared_ptr<std::pmr::memory_resource> m_mr;

  /*! \brief packet allocator
   *
   * uses m_mr as its memory resource
   */
  std::pmr::polymorphic_allocator<fpacket_type> m_packet_allocator;

  /*! \brief map from stream (channel) offsets to endpoints in the radio camera
   * processor */
  std::map<rcp::dc::channel_type, std::shared_ptr<EndpointAddr>> m_rc_endpoints;

  /*! \brief request message type to send packets */
  struct SendPackets {
    /*! \brief initial timestamp */
    std::chrono::nanoseconds t0;
    /*! \brief samples */
    L::PhysicalRegion const* samples_region;
    /*! \brief packet send completion action
     *
     * Upon completion of sending all packets in the region, either a promise is
     * fulfilled, or a callback is invoked with the number of packets
     * successfully sent.
     */
    std::variant<std::promise<unsigned>, std::function<void(unsigned)>> promise;
  };
  /*! \brief request message type to stop service thread */
  struct Stop {};
  /*! \brief union of request message types for service thread */
  using request_type = std::variant<SendPackets, Stop>;

  /*! \brief background thread request queue */
  std::queue<request_type> m_requests;

  /*! \brief mutex for values shared by service thread and other threads */
  std::mutex m_mtx;

  /*! \brief service thread */
  std::thread m_thread;

  /*! \brief file descriptor for service request queue */
  int m_request_fd;

  /*! \brief OFI message prefix size */
  std::size_t m_packet_offset;

  /*! \brief whether or not packets are sent to multicast addresses */
  bool m_multicast_packets;

  /*! \brief port for multicast transmissions from RCP that send destination
   * addresses (not used when packets are sent by multicast) */
  in_port_t m_mc_port{0};

  /*! \brief multicast address for transmissions from RCP that send destination
   * addresses (not used when packets are sent by multicast) */
  in_addr m_mc_addr{};

  /*! \brief interval between progress reports to log */
  std::chrono::seconds m_progress_report_interval;

  /*! \brief time of most recent progress report*/
  std::chrono::time_point<std::chrono::system_clock> m_last_progress_report;

  /*! \brief total number of packets sent */
  std::size_t m_num_packets_sent;

  /*! \brief service thread function */
  virtual void
  run() = 0;

  /*! \brief utility for managing file descriptors that are polled by the
   * service thread */
  struct PollFds {
    static auto constexpr request_index = 0;
    static auto constexpr send_index = 1;
    static auto constexpr mcast_index = 2;
    std::array<pollfd, 3> pfds;
    PollFds(int request_fd, int send_fd, int mcast_fd) {
      pfds[request_index] = {.fd = request_fd, .events = POLLIN};
      pfds[send_index] = {.fd = send_fd, .events = POLLIN};
      pfds[mcast_index] = {.fd = mcast_fd, .events = POLLIN};
    }
    auto
    request() -> pollfd& {
      return pfds[request_index];
    }
    auto
    send() -> pollfd& {
      return pfds[send_index];
    }
    auto
    mcast() -> pollfd& {
      return pfds[mcast_index];
    }
    auto
    toggle_request() -> void {
      pfds[request_index].fd *= -1;
    }
    auto
    toggle_send() -> void {
      pfds[send_index].fd *= -1;
    }
    auto
    toggle_mcast() -> void {
      pfds[mcast_index].fd *= -1;
    }
    auto
    request_in() -> bool {
      return pfds[request_index].revents & POLLIN;
    }
    auto
    send_in() -> bool {
      return pfds[send_index].revents & POLLIN;
    }
    auto
    mcast_in() -> bool {
      return pfds[mcast_index].revents & POLLIN;
    }
  };

public:
  /*! \brief FSamplesRegion type alias */
  using md_t = FSamplesRegion;

  /*! \brief constructor */
  template <rcp::dc::HasSingleBlockMemoryResource M>
  FEngineBase(
    Configuration const& config,
    std::shared_ptr<M> const& sbmr,
    oficpp::FiInfo const& info)
    : m_mr(
        OFIUtil::requires_tx_message_prefix(info)
          ? std::static_pointer_cast<std::pmr::memory_resource>(
              std::make_shared<rcp::dc::prefixed_single_block_pool>(
                OFIUtil::tx_message_prefix_size(info), sbmr.get()))
          : std::static_pointer_cast<std::pmr::memory_resource>(sbmr))
    , m_request_fd(eventfd(0, 0))
    , m_packet_offset(OFIUtil::tx_message_prefix_size(info))
    , m_multicast_packets(config.multicast_packets)
    , m_progress_report_interval(config.progress_report_interval())
    , m_num_packets_sent(0) {

    if (!m_multicast_packets) {
      auto [node, port] = parse_multicast_option(config.multicast_address);
      m_mc_port = htons(port);
      [[maybe_unused]] auto rc = inet_pton(AF_INET, node.c_str(), &m_mc_addr);
      assert(rc == 1);
    }
  }

  /*! \brief destructor
   *
   * Will stop service thread if it hasn't already been stopped
   */
  virtual ~FEngineBase();

  /*! \brief start the service thread */
  auto
  start() -> void;

  /*! \brief stop the service thread */
  auto
  stop() -> void;

  /*! \brief send sample packets, without completion callback
   *
   * \param t0 timestamp of first sample in region
   * \param samples_region
   * \return future holding true iff service thread is running
   */
  auto
  send(
    std::chrono::nanoseconds::rep t0,
    L::PhysicalRegion const& samples_region) -> std::future<unsigned>;

  /*! \brief send sample packets, with completion callback */
  auto
  send(
    std::chrono::nanoseconds::rep t0,
    L::PhysicalRegion const& samples_region,
    std::function<void(unsigned)> const& cb) -> void;

  /*! \brief number of packets needed to send data in a samples region */
  static auto
  num_packets(L::PhysicalRegion const& samples_region) -> unsigned;

  /*! \brief split an address string on colon, returning IP and port as strings
   */
  static auto
  split_multicast_option(std::string const& str) -> std::array<std::string, 2>;

  /*! \brief parse an address string, returning IP as string and port as
   * unsigned integer (in host byte order) */
  static auto
  parse_multicast_option(std::string const& str)
    -> std::tuple<std::string, std::uint16_t>;
};

/*! \brief partition of FSamplesRegion along receiver axis by (control
 *  replication shard, fengine instance) indexes
 *
 * receiver range for color (shard, fe) given receivers_def_t value rd is
 * (rd[shard][0], rd[shared][0] + rd[shard][1] - 1)
 */
class SendFSamplesPartition {

  /*! \brief number of FEngines in each shard */
  std::vector<coord_t> m_num_fe_by_shard;

  /*! \brief receivers (antennas) for each shard and FEngine instance */
  std::vector<std::vector<std::array<coord_t, 2>>> m_receivers_by_shard_and_fe;

  /*! \brief partition of FSamplesRegion index space */
  FSamplesRegion::index_partition_t m_ip;

public:
  /* \brief FSamplesRegion type alias */
  using md_t = FSamplesRegion;

  /*! \brief FSamplesRegion index space type alias */
  using index_space_t = typename md_t::index_space_t;

  /*! \brief FSamplesRegion index partition type alias */
  using index_partition_t = typename md_t::index_partition_t;

  /*! \brief color space type alias for index_partition_t */
  using color_space_t = L::IndexSpaceT<2, coord_t>;

  /*! \brief default constructor (defaulted) */
  SendFSamplesPartition() = default;

  /*! \brief non-default constructor
   *
   * \param receivers_by_shard
   * \param is index space for FSamplesRegion
   */
  SendFSamplesPartition(
    L::Context ctx,
    L::Runtime* rt,
    receivers_def_t const& receivers_by_shard,
    unsigned max_fe_per_shard,
    index_space_t is);

  /*! \brief copy constructor (defaulted) */
  SendFSamplesPartition(SendFSamplesPartition const&) = default;

  /*! \brief move constructor (defaulted) */
  SendFSamplesPartition(SendFSamplesPartition&&) = default;

  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(SendFSamplesPartition const&) -> SendFSamplesPartition& = default;

  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(SendFSamplesPartition&&) -> SendFSamplesPartition& = default;

  /*! \brief samples region index partition instance */
  auto
  index_partition() const -> index_partition_t;

  /*! \brief receiver ids for each shard and FEngine instance */
  auto
  receivers_by_shard_and_fe() const
    -> std::vector<std::vector<std::array<coord_t, 2>>> const&;

  /*! \brief create partition of index_space_t for use by FEngine instances */
  auto
  make_partition(
    L::Context ctx,
    L::Runtime* rt,
    receivers_def_t const& receivers_by_shard,
    unsigned max_fe_per_shard,
    index_space_t fsamples_is) -> index_partition_t;
};

/*! \brief launcher for task to send F-engine packets */
class SendFSamples {

  /*! \brief task name */
  static constexpr char const* task_name = "SendFSamples";

  /*! \brief Legion task launcher instance */
  L::IndexTaskLauncher m_launcher;

  /*! \brief initial timestamp */
  std::shared_ptr<std::chrono::nanoseconds::rep> m_t0;

  /*! \brief blocks of memory for the FEngines (on local shard) for allocating
   * packets */
  std::vector<std::vector<char>> m_packet_blocks;

  /*! \brief memory resources for the FEngines (on local shard) to manage
   * allocations in m_packet_blocks */
  std::vector<rcp::dc::single_block_memory_resource> m_packet_mrs;

  /*! \brief pointers to FEngine instances (on local shard) */
  std::vector<std::unique_ptr<FEngineBase>> m_fes;

  /*! \brief region to maintain FEngine instances */
  L::LogicalRegionT<2> m_fe_lr;

  /*! \brief physical region mapped locally for FEngine instances */
  std::optional<L::PhysicalRegion> m_fe_pr;

  /*! \brief partition for sample regions */
  SendFSamplesPartition m_samples_partition;

  /*! \brief partition of FEngine instances by shard and instance number */
  L::IndexPartitionT<2, coord_t> m_fe_ip;

  /*! \brief future returned by most recent task launch */
  L::FutureMap m_last_launch;

public:
  /*! \brief FSamplesRegion type alias */
  using fs_md_t = FSamplesRegion;

  /*! \brief default constructor (defaulted) */
  SendFSamples() = default;

  /*! \brief main constructor */
  SendFSamples(
    L::Context ctx,
    L::Runtime* rt,
    Configuration const& config,
    std::size_t shard_point,
    receivers_def_t const& receivers_by_shard,
    fs_md_t::index_space_t index_space,
    L::MappingTagID tag);

  /*! \brief copy constructor (deleted) */
  SendFSamples(SendFSamples const&) = delete;

  /*! \brief move constructor (defaulted) */
  SendFSamples(SendFSamples&&) = default;

  /*! \brief copy assignment operator (deleted) */
  auto
  operator=(SendFSamples const&) -> SendFSamples& = delete;

  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(SendFSamples&&) -> SendFSamples& = default;

  /*! \brief FSamplesRegion partition instance */
  auto
  index_partition() const -> fs_md_t::index_partition_t;

  /*! \brief release all resources */
  auto
  destroy(L::Context ctx, L::Runtime* rt) -> void;

  /*! \brief launch task to send packets for samples in a region */
  auto
  launch(
    L::Context ctx,
    L::Runtime* rt,
    fs_md_t::LogicalRegion region,
    std::chrono::nanoseconds const& t0) -> L::FutureMap;

  /*! \brief preregister task id */
  static auto
  preregister(L::TaskID task_id) -> void;
};
} // namespace rcp::fsim

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
