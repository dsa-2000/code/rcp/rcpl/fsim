// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include <rcp/DataPacketConfiguration.hpp>
#include <rcp/dc/rcpdc.hpp>
#include <rcp/rcp.hpp>

namespace rcp::fsim {

struct Tag {};

} // namespace rcp::fsim

namespace bark {

/*! static context for bark logging in fsim namespace */
template <>
struct StaticContext<rcp::fsim::Tag> {
  static constexpr char const* nm = "fsim";
  // to avoid proliferation of configuration macros, we reuse the librcp
  // logging values
  static constexpr bark::Level min_log_level = RCP_MIN_LOG_LEVEL;
  static constexpr bool trace_location = RCP_TRACE_LOCATION;
  static constexpr bool debug_location = RCP_DEBUG_LOCATION;
  static constexpr bool info_location = RCP_INFO_LOCATION;
  static constexpr bool warn_location = RCP_WARN_LOCATION;
  static constexpr bool error_location = RCP_ERROR_LOCATION;
  static constexpr bool fatal_location = RCP_FATAL_LOCATION;
  static constexpr bool print_location = false;
  static constexpr auto
  source_identity() -> char const* {
    return nm;
  }
};

} // end namespace bark

namespace rcp {

template <>
auto
get_log_sink<fsim::Tag>() -> LogSink<fsim::Tag>&;

}

namespace rcp::fsim {

/*! \brief initialize log sink for a fsim instance */
auto
init_log_sink(std::string_view const& name) -> void;

/*!  log instance for bark logging in rcp namespace */
template <typename T = Tag>
inline auto
log() {
  return bark::Log<T>{get_log_sink<T>()};
}

/*! scope trace log instance for bark logging in rcp namespace */
template <typename T = Tag>
inline auto
trace(std::source_location loc = bark::source_location::current()) {
  return bark::ScopeTraceLog<T>(get_log_sink<T>(), loc);
}

/*! type for a sequence of receiver ranges
 *
 * array values are start (offset) and size of receiver range
 */
using receivers_def_t =
  std::vector<std::array<rcp::DataPacketConfiguration::receiver_type, 2>>;

/*! \brief samples layout */
auto
samples_layout() -> PortableLayout<> const&;

} // namespace rcp::fsim

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
