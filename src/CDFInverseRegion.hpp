// Copyright 2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "Configuration.hpp"
#include <rcp/rcp.hpp>

namespace rcp::fsim {

/* \brief axes for CDF inverse regions */
enum class CDFInverseAxes {
  probability = LEGION_DIM_0 /*!< unnormalized probability */
};

/*! \brief IndexSpec specialization for CDFInverseAxes
 *
 * \tparam Coord index coordinate value type
 */
template <typename Coord>
using cdf_inverse_index_spec_t = IndexSpec<
  CDFInverseAxes,
  CDFInverseAxes::probability,
  CDFInverseAxes::probability,
  Coord>;

/*! \brief default static bounds for regions with CDFInverseAxes
 *
 * \tparam Coord index coordinate value type
 *
 * Bounds are the limits of Configuration::gen_type type
 */
template <typename Coord>
  requires(std::is_unsigned_v<Coord>
           && sizeof(Coord) >= sizeof(Configuration::gen_type))
constexpr auto default_cdf_inverse_bounds =
  std::array{IndexInterval<Coord>::bounded(
    std::numeric_limits<Configuration::gen_type>::min(),
    std::numeric_limits<Configuration::gen_type>::max())};

/*! \brief field for inverse CDF of real part of sample values*/
using CDFRealPartField = StaticField<int, 77>;
/*! \brief field for inverse CDF of imaginary part of sample values*/
using CDFImagPartField = StaticField<int, 78>;

/*! \brief sample CDF inverse mapping region
 *
 * CDFInverseRegion provides the inverse of the mapping from sample
 * value parts (real and imaginary) to the unnormalized cumulative
 * probability of that part of a sample. The unnormalized probability
 * scale is determined by gen_type (value
 * std::numeric_limits<gen_type>::min() is probability 0, and value
 * std::numeric_limits<gen_type>::max() is probability 1.0), which
 * allows the inverse mapping to go directly from RNG values (of
 * gen_type) to the real or imaginary part of sample values.
 */
struct CDFInverseRegion
  : public StaticRegionSpec<
      cdf_inverse_index_spec_t<unsigned>,
      default_cdf_inverse_bounds<unsigned>,
      CDFRealPartField,
      CDFImagPartField> {};

} // namespace rcp::fsim

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
