// Copyright 2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "CDFInverseRegion.hpp"
#include "Configuration.hpp"

#include <rcp/rcp.hpp>

namespace rcp::fsim {

/*! \brief task launcher to initialize sample distributions
 *
 * Specifically, a sample distribution is computed from a pair of probability
 * mass functions for the real and imaginary sample values, and the distribution
 * is represented by an instance of CDFInverseRegion.
 */
class InitSampleDistributions {
public:
  /*! \brief task argument type */
  struct Args {
    /*! \brief probability mass function for real part of sample values */
    std::array<float, Configuration::num_sample_part_values> real_pmf;
    /*! \brief probability mass function for imaginary part of sample values */
    std::array<float, Configuration::num_sample_part_values> imag_pmf;
  };

private:
  /*! \brief alias for CDFInverseRegion */
  using icdf_t = CDFInverseRegion;

  /*! \brief generator for CDFInverseRegion instances */
  DenseArrayRegionGenerator<icdf_t> m_gen;

  /*! \brief task argument value */
  Args m_args;

public:
  /*! \brief default constructor (defaulted) */
  InitSampleDistributions() = default;

  /*! \brief constructor
   *
   * Uses probability mass functions from config.
   */
  InitSampleDistributions(
    L::Context ctx, L::Runtime* rt, Configuration const& config);

  /*! \brief copy constructor (deleted) */
  InitSampleDistributions(InitSampleDistributions const&) = delete;

  /*! \brief move constructor (defaulted) */
  InitSampleDistributions(InitSampleDistributions&&) = default;

  /*! \brief destructor */
  ~InitSampleDistributions() = default;

  /*! \brief copy assignment (deleted) */
  auto
  operator=(InitSampleDistributions const&)
    -> InitSampleDistributions& = delete;

  /*! \brief move assignment (defaulted) */
  auto
  operator=(InitSampleDistributions&&) -> InitSampleDistributions& = default;

  /*! \brief launch task to initialize a sample distribution region */
  auto
  launch(L::Context ctx, L::Runtime* rt, icdf_t::LogicalRegion inverse_cdf)
    -> void;

  /*! \brief create a new sample distribution region */
  auto
  new_inverse_cdf_region(L::Context ctx, L::Runtime* rt)
    -> icdf_t::LogicalRegion;

  /*! \brief free resources */
  auto
  destroy(L::Context ctx, L::Runtime* rt) -> void;

  /*! \brief preregister the task with a given id */
  static auto
  preregister(L::TaskID id) -> void;
};
} // namespace rcp::fsim

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
