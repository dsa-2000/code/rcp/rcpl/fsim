// Copyright 2023,2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "SendFSamples.hpp"

#include <algorithm>
#include <array>
#include <functional>
#include <oficpp.hpp>
#include <ranges>

#include <arpa/inet.h>
#include <fcntl.h>
#include <poll.h>
#include <rcp/DataPacketConfiguration.hpp>
#include <rcp/rcp.hpp>
#include <rdma/fabric.h>
#include <string>
#include <sys/eventfd.h>
#include <unistd.h>

namespace {
/*! \brief map packet channel offset to sender multicast group port
 * number
 */
auto
multicast_port(rcp::dc::channel_type ch_offset, std::uint16_t port_offset)
  -> std::uint16_t {

  return std::uint16_t(
           ch_offset / rcp::DataPacketConfiguration::num_channels_per_packet)
         + port_offset;
}
} // namespace

namespace rcp::fsim {

/*! \brief wrapper for a packet plus an OFI context */
template <unsigned N>
struct FPacketContext {
  std::array<fi_context, N> hdr;
  fpacket_type* packet;
};

/*! \brief FEngine class for a fixed OFI message context size */
template <unsigned ContextSz>
class FEngine : public FEngineBase {

  /*! \brief representation of FEngine endpoint */
  struct Endpoint {

    /*! \brief OFI endpoint */
    std::shared_ptr<oficpp::FidEndpoint<FPacketContext<ContextSz>>> ep;
    /*! \brief OFI registered memory region */
    std::shared_ptr<oficpp::FidMr<>> mr;
    /* \brief OFI completion queue */
    std::shared_ptr<oficpp::FidCq<>> cq;
    /* \brief OFI address vector */
    std::shared_ptr<oficpp::FidAv<>> av;

    /*! \brief constructor
     *
     * \param config Configuration instance
     * \param memory_resource_buffer address of beginning of memory region for
     *        messages
     * \param memory_resource_size total size of memory region for messages
     * \param info fabric info
     *
     * All memory from memory_resource_buffer to memory_resource_buffer +
     * memory_resource_size is registered for libfabric, if needed by the
     * fabric.
     */
    Endpoint(
      Configuration const& config,
      void* memory_resource_buffer,
      std::size_t memory_resource_size,
      oficpp::FiInfo info) {

      auto fabric = oficpp::FidFabric<>::create(info);
      fi_eq_attr eq_attr{
        .size = 0,
        .flags = 0,
        .wait_obj = FI_WAIT_NONE,
        .signaling_vector = 0,
        .wait_set = nullptr};
      oficpp::FidEq<>::create(fabric, eq_attr);
      auto domain = oficpp::FidDomain<>::create(fabric, info);
      ep = oficpp::FidEndpoint<FPacketContext<ContextSz>>::create(domain, info);
      fi_cq_attr cq_attr{
        .size = config.completion_queue_size,
        .flags = 0,
        .format = FI_CQ_FORMAT_CONTEXT,
        .wait_obj = FI_WAIT_FD,
        .signaling_vector = 0,
        .wait_cond = FI_CQ_COND_NONE,
        .wait_set = nullptr};
      cq = oficpp::FidCq<>::create(domain, cq_attr);
      ep->bind(cq, FI_SEND);
      if ((info->domain_attr->mr_mode & FI_MR_LOCAL) != 0) {
        mr = oficpp::FidMr<>::create(
          domain,
          memory_resource_buffer,
          memory_resource_size,
          FI_SEND,
          0,
          OFIUtil::mr_key,
          0);
        if ((info->domain_attr->mr_mode & FI_MR_ENDPOINT) != 0) {
          mr->bind(ep);
          mr->enable();
        }
      }
      fi_av_attr av_attr{
        .type = info->domain_attr->av_type,
        .rx_ctx_bits = 0,
        .count =
          (config.num_sample_channels
           / DataPacketConfiguration::num_channels_per_packet),
        .ep_per_node = 0, // TODO: optimize?
        .name = nullptr,
        .map_addr = nullptr,
        .flags = 0};
      av = oficpp::FidAv<>::create(domain, av_attr);
      ep->bind(av);
      ep->enable();
    }
  };

  /*! \brief OFI endpoint for the FEngine */
  Endpoint m_endpoint;

  /*! \brief message contexts
   *
   * Once a context is put on the stack, it can be reused, and will not be
   * destroyed for the life of the FEngine.
   */
  std::stack<FPacketContext<ContextSz>*> m_packet_contexts;

  /*! \brief buffer for message send completions
   *
   * use is transient, nothing is stored in this vector beyond its use in a
   * block within run()
   */
  std::vector<FPacketContext<ContextSz>*> m_tx_completions;

  /* \brief file descriptor for completion queue */
  auto
  send_fd() -> int {

    int result;
    auto rc = fi_control(*m_endpoint.cq, FI_GETWAIT, &result);
    if (rc != 0)
      oficpp::abort_fi(rc);
    return result;
  }

  /*! \brief send the samples in a region */
  auto
  send_packets(
    std::chrono::nanoseconds::rep t0,
    L::PhysicalRegion const& samples_region) -> unsigned {

    md_t::rect_t samples_bounds(samples_region);
    // std::cout << "samples bounds: " << samples_bounds << '\n';
    md_t::rect_t packet_bounds = samples_bounds;
    auto const samples =
      md_t::values<LEGION_READ_ONLY>(samples_region, FSampleField{});

    // {
    //   auto lo = samples_bounds.lo;
    //   auto hi = samples_bounds.lo;
    //   hi[dim(md_t::axes_t::receiver)] = lo[dim(md_t::axes_t::receiver)] + 1;
    //   std::cout << "receiver stride: " << samples.ptr(hi) - samples.ptr(lo)
    //             << '\n';
    //   hi[dim(md_t::axes_t::receiver)] = lo[dim(md_t::axes_t::receiver)];
    //   hi[dim(md_t::axes_t::channel)] = lo[dim(md_t::axes_t::channel)] + 1;
    //   std::cout << "channel stride: " << samples.ptr(hi) - samples.ptr(lo)
    //             << '\n';
    //   hi[dim(md_t::axes_t::channel)] = lo[dim(md_t::axes_t::channel)];
    //   hi[dim(md_t::axes_t::polarization)] =
    //     lo[dim(md_t::axes_t::polarization)] + 1;
    //   std::cout << "polarization stride: " << samples.ptr(hi) -
    //   samples.ptr(lo)
    //             << '\n';
    //   hi[dim(md_t::axes_t::polarization)] =
    //   lo[dim(md_t::axes_t::polarization)]; hi[dim(md_t::axes_t::timestep)] =
    //   lo[dim(md_t::axes_t::timestep)] + 1; std::cout << "timestep stride: "
    //   << samples.ptr(hi) - samples.ptr(lo)
    //             << '\n';
    //   std::cout << "timestep pdiff: "
    //             << reinterpret_cast<char const*>(samples.ptr(hi))
    //                  - reinterpret_cast<char const*>(samples.ptr(lo))
    //             << '\n';
    // }

    unsigned result{0};
    for (coord_t ts = samples_bounds.lo[dim(md_t::axes_t::timestep)];
         ts <= samples_bounds.hi[dim(md_t::axes_t::timestep)];
         ts += DataPacketConfiguration::num_timesteps_per_packet) {
      packet_bounds.lo[dim(md_t::axes_t::timestep)] = ts;
      packet_bounds.hi[dim(md_t::axes_t::timestep)] =
        ts + DataPacketConfiguration::num_timesteps_per_packet - 1;
      for (auto&& rcvr :
           rcp::range(samples_bounds, dim(md_t::axes_t::receiver))) {
        packet_bounds.lo[dim(md_t::axes_t::receiver)] = rcvr;
        packet_bounds.hi[dim(md_t::axes_t::receiver)] = rcvr;
        for (coord_t ch = samples_bounds.lo[dim(md_t::axes_t::channel)];
             ch <= samples_bounds.hi[dim(md_t::axes_t::channel)];
             ch += DataPacketConfiguration::num_channels_per_packet) {
          if (m_rc_endpoints.contains(ch)) {
            packet_bounds.lo[dim(md_t::axes_t::channel)] = ch;
            packet_bounds.hi[dim(md_t::axes_t::channel)] =
              ch + DataPacketConfiguration::num_channels_per_packet - 1;
            // get send buffer, then initialize header fields
            // std::cout << "packet bounds: " << packet_bounds << '\n';
            auto packet = m_packet_allocator.allocate(1);
            packet->receiver = rcvr;
            packet->timestamp_offset =
              t0
              + ts
                  * std::chrono::nanoseconds(
                      DataPacketConfiguration::fsample_interval)
                      .count();
            packet->channel_offset = ch;
            // Copy samples to packet. We get by with a single memcpy because
            // the layout of samples_region matches the layout of FPacket.
            std::memcpy(
              &packet->fsamples_mdspan()(0, 0, 0),
              samples.ptr(packet_bounds),
              packet_bounds.volume() * sizeof(FSampleField::value_t));
            // send packet
            char* buffer = reinterpret_cast<char*>(packet) - m_packet_offset;
            auto buf =
              std::span(buffer, m_packet_offset + sizeof(fpacket_type));
            if (m_packet_contexts.empty())
              m_packet_contexts.push(new FPacketContext<ContextSz>());
            auto ctx = m_packet_contexts.top();
            m_packet_contexts.pop();
            ctx->packet = packet;
            m_endpoint.ep->send(
              buf,
              (m_endpoint.mr ? m_endpoint.mr->desc() : nullptr),
              m_rc_endpoints.at(ch)->addr(),
              ctx);
            ++result;
          }
        }
      }
    }
    // report the latency of packet transmissions vs timestamps of samples...it
    // is only one, imperfect, metric, but it should suffice to gauge
    // performance
    auto now = std::chrono::system_clock::now().time_since_epoch().count();
    auto last =
      t0
      + (std::chrono::duration_cast<std::chrono::nanoseconds>(DataPacketConfiguration::fsample_interval).count() * DataPacketConfiguration::num_timesteps_per_packet);
    log().info<long>("send latency", {"ns", now - last});
    return result;
  }

  /*! \brief read a message from rc streams multicast socket */
  auto
  read_mcast(int fd)
    -> std::optional<rcp::dc::Message<Configuration::max_streams_per_dc>> {

    rcp::dc::Message<Configuration::max_streams_per_dc> msg;
    auto rc = read(fd, &msg, sizeof(msg));
    if (!(rc == -1 && (errno == EAGAIN || errno == EWOULDBLOCK))) {
      assert(rc == sizeof(msg));
      return msg;
    }
    return std::nullopt;
  }

  /*! \brief read from an eventfd file descriptor
   *
   * read value is ignored
   */
  auto
  ack_eventfd(int fd) -> void {
    eventfd_t e;
    [[maybe_unused]] auto rc = eventfd_read(fd, &e);
    // assert(rc == 0);
  }

  /*! \brief send packets in requests with timestamps not later than a given
   * time
   *
   * \param t upper limit of timestamps of requests for which to send packets
   * \param packets time-ordered sequence of SendPackets instances
   *
   * \return time-ordered sequence of SendPackets instances lacking requests
   *         with timestamps not later than t, and the number of packets sent
   */
  auto
  send_up_to(std::chrono::nanoseconds t_end, std::vector<SendPackets>&& packets)
    -> std::tuple<std::vector<SendPackets>, unsigned> {

    unsigned num_sent{0};
    auto pkt = packets.begin();
    while (pkt != packets.end() && pkt->t0 <= t_end) {
      num_sent += send_packets(pkt->t0.count(), *pkt->samples_region);
      std::visit(
        overloaded{
          [&](std::promise<unsigned>& pr) { pr.set_value(num_sent); },
          [&](std::function<void(unsigned)>& fn) { fn(num_sent); }},
        pkt->promise);
      ++pkt;
    }
    std::vector<SendPackets> remaining;
    std::ranges::move(pkt, packets.end(), std::back_inserter(remaining));
    return {std::move(remaining), std::move(num_sent)};
  }

  /*! \brief main service thread loop
   *
   * Polls three types of events: service requests (from application thread),
   * message send completions (from completion queue), and receipt of multicast
   * messages from the rc multicast
   */
  auto
  main_loop(PollFds& pollfds) -> void {

    using namespace std::string_literals;

    auto constexpr poll_timeout =
      std::chrono::ceil<std::chrono::milliseconds>(
        std::chrono::nanoseconds(DataPacketConfiguration::fsample_interval) / 2)
        .count();

    std::vector<SendPackets> unsent_packets;
    bool end_loop = false;
    while (!end_loop) {
      {
        int rc = poll(pollfds.pfds.data(), pollfds.pfds.size(), poll_timeout);
        if (rc < 0)
          log().fatal("Error in call to poll(): "s + std::strerror(errno));
      }
      // send-completion events
      if (pollfds.send_in()) {
        ack_eventfd(pollfds.send().fd);
        // read completions from the completion queue
        ssize_t n;
        bool retry;
        do {
          n = m_endpoint.cq->read(std::span(m_tx_completions));
          if (n == -FI_EAVAIL) {
            fi_cq_err_entry cq_err_entry;
            [[maybe_unused]] auto rc = m_endpoint.cq->readerr(cq_err_entry);
            assert(rc == 0);
            log().info(
              "Transmit error on completion queue: "s
              + fi_strerror(cq_err_entry.err));
          } else if (n == -FI_EAGAIN) {
            n = 0;
          }
          retry = (n == -FI_EAVAIL) || (n == -FI_EINTR);
        } while (retry);
        // deallocate packets associated with every completion queue element,
        // and stuff the contexts onto m_packet_contexts for eventual reuse
        if (n > 0)
          for (auto&& i : std::views::iota(std::size_t{0}, std::size_t(n))) {
            m_packet_allocator.deallocate(m_tx_completions[i]->packet, 1);
            m_packet_contexts.push(m_tx_completions[i]);
          }
      }
      // rc multicast message receive events
      if (pollfds.mcast_in()) {
        assert(!m_multicast_packets);
        // received radio camera multicast message(s), process them all
        auto msg = read_mcast(pollfds.mcast().fd);
        while (msg) {
          // look for a match among currently known rc endpoints
          auto ch_ea = std::ranges::find_if(m_rc_endpoints, [&](auto&& ch_ep) {
            return ch_ep.second->name() == msg->endpoint_name;
          });
          std::shared_ptr<EndpointAddr> ea;
          if (ch_ea == m_rc_endpoints.end()) {
            // create a new EndpointAddr if the name in the message is unknown
            ea =
              std::make_shared<EndpointAddr>(msg->endpoint_name, m_endpoint.av);
          } else {
            // reference an already existing EndpointAddr
            ea = ch_ea->second;
          }
          // associate all (valid) channels listed in the message with the
          // EndpointAddr
          std::ranges::for_each(
            std::views::transform(
              msg->channel_offsets, [](auto&& ch) { return ntohs(ch); })
              | std::views::take_while(
                [&](auto&& ch) { return ch != msg->invalid_channel_offset; }),
            [&, this](auto&& ch) { m_rc_endpoints[ch] = ea; });
          // try to read another multicast message
          msg = read_mcast(pollfds.mcast().fd);
        }
      }
      // service request events
      if (pollfds.request_in()) {
        ack_eventfd(pollfds.request().fd);
        // copy the current requests to minimize the time we must hold the mutex
        decltype(m_requests) requests;
        {
          std::scoped_lock lk(m_mtx);
          requests = std::move(m_requests);
        }
        // process the requests
        while (requests.size() > 0) {
          std::visit(
            overloaded{
              [&](SendPackets& r) {
                unsent_packets.emplace_back(std::move(r));
              },
              [&](Stop&) { end_loop = true; }},
            requests.front());
          requests.pop();
        }
      }
      auto now = std::chrono::system_clock::now();
      std::size_t num_sent;
      std::tie(unsent_packets, num_sent) =
        send_up_to(now.time_since_epoch(), std::move(unsent_packets));
      m_num_packets_sent += num_sent;
      log().print_if<std::uintptr_t, std::size_t>(
        [&, this]() {
          if (now - m_last_progress_report >= m_progress_report_interval) {
            m_last_progress_report = now;
            return true;
          }
          return false;
        },
        "FEngine",
        {"id", reinterpret_cast<std::uintptr_t>(this)},
        {"total packets sent", m_num_packets_sent});
    }
  }

  /*! \brief service thread
   *
   * Begins by creating a socket to listen for rc streams multicast messages,
   * and then goes into the main loop, handling requests and events.
   *
   * \see main_loop()
   */
  auto
  run() -> void override {
    using namespace std::string_literals;

    int mcast_fd = -1;
    if (!m_multicast_packets) {
      // open a socket that can receive from the rc multicast group
      mcast_fd = socket(AF_INET, SOCK_DGRAM | SOCK_NONBLOCK, 0);
      assert(mcast_fd != -1);
      {
        int reuse = 1;
        [[maybe_unused]] auto rc =
          setsockopt(mcast_fd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse));
        assert(rc == 0);
      }
      {
        sockaddr_in addr{
          .sin_family = AF_INET,
          .sin_port = m_mc_port,
          .sin_addr = {.s_addr = INADDR_ANY}};
        [[maybe_unused]] auto rc =
          bind(mcast_fd, reinterpret_cast<sockaddr*>(&addr), sizeof(addr));
        assert(rc == 0);
      }
      {
        ip_mreqn const mreqn{
          .imr_multiaddr = m_mc_addr,
          .imr_address = {.s_addr = INADDR_ANY},
          .imr_ifindex = 0};
        [[maybe_unused]] auto rc = setsockopt(
          mcast_fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreqn, sizeof(mreqn));
        assert(rc == 0);
      }
    }

    PollFds pollfds(m_request_fd, send_fd(), mcast_fd);
    main_loop(pollfds);

    // close the socket to avoid accumulating messages when they are not needed
    if (mcast_fd > 0 && close(mcast_fd) != 0)
      log().warn(
        "Close of rc multicast message socket file descriptor failed: "s
        + std::strerror(errno));
  }

public:
  /*! \brief constructor
   *
   * \see FEngineBase::FEngineBase()
   */
  template <rcp::dc::HasSingleBlockMemoryResource M>
  FEngine(
    Configuration const& config,
    std::shared_ptr<M> const& sbmr,
    oficpp::FiInfo const& info)
    : FEngineBase(config, sbmr, info)
    , m_endpoint(config, sbmr->address(), sbmr->size(), info)
    , m_tx_completions(config.completion_queue_size) {
    if (m_multicast_packets) {
      // in this case, we're sending packets to multicast addresses, which we
      // own
      auto [node, base_port] = parse_multicast_option(config.multicast_address);
      for (dc::channel_type ch = 0; ch < config.num_sample_channels;
           ch += DataPacketConfiguration::num_channels_per_packet) {
        auto port = std::to_string(multicast_port(ch, base_port));
        oficpp::FiInfo hints;
        hints->caps = FI_MSG | FI_MULTICAST | FI_SEND;
        hints->mode = ~0;
        hints->fabric_attr->prov_name = strdup(config.fabric_provider.c_str());
        hints->ep_attr->type = FI_EP_DGRAM;
        auto infos = hints.getinfo(
          OFIUtil::fi_version,
          node.c_str(),
          port.c_str(),
          FI_MSG | FI_MULTICAST | FI_SEND);
        assert(infos.size() == 1);
        auto const& info = infos.front();
        assert(info->dest_addr != nullptr);
        auto nm = std::vector<char>(info->dest_addrlen);
        std::copy(
          static_cast<char*>(info->dest_addr),
          static_cast<char*>(info->dest_addr) + info->dest_addrlen,
          nm.begin());
        dc::EndpointName endpoint_name(infos.front()->addr_format, nm);
        m_rc_endpoints[ch] = std::make_shared<EndpointAddr>(
          dc::EndpointName(infos.front()->addr_format, nm), m_endpoint.av);
      }
    }
  }

  /*! \brief destructor */
  virtual ~FEngine() {
    m_rc_endpoints.clear();
    while (!m_packet_contexts.empty()) {
      delete m_packet_contexts.top();
      m_packet_contexts.pop();
    }
  }
};

/*! \brief factory class for FEngine instances
 *
 * Exists to allow client code to avoid dealing directly with specializations of
 * FEngine.
 */
class FEngineFactory {

  /*! \brief check whether the given info instance requires a context of value
   * ctx */
  static auto
  check_context(oficpp::FiInfo const& info, decltype(fi_info::mode) const& ctx)
    -> bool {

    return (((info->tx_attr->mode & ctx) != 0) || ((info->mode & ctx) != 0));
  }

public:
  /*! \brief create an FEngine instance
   *
   * \see FEngineBase::FEngineBase()
   */
  template <rcp::dc::HasSingleBlockMemoryResource M>
  static auto
  create(
    Configuration const& config,
    std::shared_ptr<M> const& sbmr,
    oficpp::FiInfo const& info) -> std::unique_ptr<FEngineBase> {

    if (check_context(info, FI_CONTEXT2))
      return std::make_unique<FEngine<2>>(config, sbmr, info);
    else if (check_context(info, FI_CONTEXT))
      return std::make_unique<FEngine<1>>(config, sbmr, info);
    else
      return std::make_unique<FEngine<0>>(config, sbmr, info);
  }
};

/*! \brief description of region for FEngine instances
 *
 * \todo update this to a rcp::StaticRegionSpec type
 */
struct FEngineRegionMetadata {

  enum Axes { shard, fe };
  static constexpr int dim = fe + 1;

  using fe_ptr_t = FEngineBase*;
  static constexpr L::FieldID fe_ptr_fid = 1;

  using index_space_t = L::IndexSpaceT<dim, coord_t>;
  using index_partition_t = L::IndexPartitionT<dim, coord_t>;
  using region_t = L::LogicalRegionT<dim, coord_t>;
  using logical_partition_t = L::LogicalPartitionT<dim, coord_t>;
  using domain_t = L::DomainT<dim, coord_t>;

  template <L::PrivilegeMode Mode>
  static auto
  fe_ptrs(L::PhysicalRegion const& pr) {

    return L::FieldAccessor<
      Mode,
      fe_ptr_t,
      dim,
      coord_t,
      L::AffineAccessor<fe_ptr_t, dim, coord_t>>(pr, fe_ptr_fid);
  }
};

// Allocator adaptor that interposes construct() calls to
// convert value initialization into default initialization.
template <typename T, typename A = std::allocator<T>>
class default_init_allocator : public A {
  using a_t = std::allocator_traits<A>;

public:
  template <typename U>
  struct rebind {
    using other =
      default_init_allocator<U, typename a_t::template rebind_alloc<U>>;
  };

  using A::A;

  template <typename U>
  void
  construct(U* ptr) noexcept(std::is_nothrow_default_constructible<U>::value) {
    ::new (static_cast<void*>(ptr)) U;
  }
  template <typename U, typename... Args>
  void
  construct(U* ptr, Args&&... args) {
    a_t::construct(static_cast<A&>(*this), ptr, std::forward<Args>(args)...);
  }
};

/*! \brief task implementation of SendFSamples */
struct SendFSamplesTask
  : public SerialOnlyTaskMixin<SendFSamplesTask, unsigned> {

  /*! \brief task name */
  static constexpr char const* task_name = "SendFSamplesTask";

  /*! \brief task id */
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  /*! \brief region indexes */
  enum { fe_ptr_region_index = 0, send_region_index, num_regions };

  /*! \brief FSamplesRegion type alias*/
  using fs_md_t = FSamplesRegion;

  /*! \brief FEngineRegionMetadata type alias*/
  using fe_md_t = FEngineRegionMetadata;

  /*! \brief implementation function */
  static auto
  serial_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> result_t {

    assert(regions.size() == num_regions);
    assert(task->index_point.get_dim() == 2);
    auto fe_ptr = fe_md_t::fe_ptrs<LEGION_READ_WRITE>(
      regions[fe_ptr_region_index])[L::Point<2>(task->index_point)];
    assert(task->arglen == sizeof(std::chrono::nanoseconds::rep));
    std::chrono::nanoseconds::rep const& t0 =
      *reinterpret_cast<std::chrono::nanoseconds::rep const*>(task->args);

    // use an atomic type here to prevent memory access order issues (not sure
    // that handshake methods make any guarantees)
    std::atomic_uint result{0};
    auto handshake = L::Runtime::create_external_handshake(true, 1);
    auto completion_cb = [&](unsigned send_result) -> void {
      result += send_result;
      handshake.ext_handoff_to_legion();
    };
    auto const& send_region = regions[send_region_index];
    fe_ptr->send(t0, send_region, completion_cb);

    handshake.legion_wait_on_ext();
    log().info_if(
      [&]() { return FEngineBase::num_packets(send_region) != result; },
      "Some packets not sent");
    return result;
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));

    using traits = task_variant_traits<Variant>;
    registrar.set_leaf();
    registrar.add_layout_constraint_set(
      SendFSamplesTask::send_region_index,
      samples_layout().constraint_id(traits::layout_style).value());
    L::Runtime::preregister_task_variant<result_t, body<Variant>>(
      registrar, task_name);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::TaskID SendFSamplesTask::task_id;

// set OFI version used throughout
int const OFIUtil::fi_version = FI_VERSION(1, 15);

// (constant) registered memory key
std::uint64_t const OFIUtil::mr_key = 1234;

auto
OFIUtil::find_fabric_interfaces(std::string const& provider)
  -> std::vector<oficpp::FiInfo> {

  oficpp::FiInfo hints;
  hints->caps = FI_MSG;
  hints->mode = ~0;
  hints->domain_attr->mode = ~0;
  hints->domain_attr->mr_mode = ~(FI_MR_BASIC | FI_MR_SCALABLE);
  hints->fabric_attr->prov_name = strdup(provider.c_str());
  hints->ep_attr->type = FI_EP_DGRAM;
  hints->ep_attr->max_msg_size = sizeof(fpacket_type);
  hints->caps |= FI_SEND;
  return hints.getinfo(fi_version);
}

EndpointAddr::EndpointAddr(
  rcp::dc::EndpointName name, std::shared_ptr<oficpp::FidAv<>> const& av)
  : m_name(name), m_av(av) {
  m_addr = m_av->insert(m_name.name)[0];
}

EndpointAddr::EndpointAddr(EndpointAddr&& other) noexcept {
  m_name = other.m_name;
  m_addr = other.m_addr;
  m_av = other.m_av;
  other.m_av.reset();
}

EndpointAddr&
EndpointAddr::operator=(EndpointAddr&& rhs) {
  std::swap(m_name, rhs.m_name);
  std::swap(m_addr, rhs.m_addr);
  std::swap(m_av, rhs.m_av);
  return *this;
}

EndpointAddr::~EndpointAddr() {
  if (m_av)
    m_av->remove(m_addr);
}

auto
EndpointAddr::name() const -> rcp::dc::EndpointName const& {
  return m_name;
}

auto
EndpointAddr::addr() const -> fi_addr_t const& {
  return m_addr;
}

FEngineBase::~FEngineBase() {
  using namespace std::string_literals;
  stop();
  if (close(m_request_fd) != 0)
    log().warn(
      "Close of eventfd request file descriptor failed: "s
      + std::strerror(errno));
}

auto
FEngineBase::start() -> void {
  std::scoped_lock lk(m_mtx);
  if (!m_thread.joinable())
    m_thread = std::thread([this]() { this->run(); });
}

auto
FEngineBase::stop() -> void {
  std::unique_lock lk(m_mtx);
  if (m_thread.joinable()) {
    m_requests.push(Stop());
    [[maybe_unused]] auto rc = eventfd_write(m_request_fd, 1);
    assert(rc == 0);
    lk.unlock();
    m_thread.join();
  } else {
    lk.unlock();
  }
}

auto
FEngineBase::send(
  std::chrono::nanoseconds::rep t0,
  L::PhysicalRegion const& samples_region) -> std::future<unsigned> {

  SendPackets request;
  request.t0 = std::chrono::nanoseconds{t0};
  request.samples_region = &samples_region;
  auto result = std::get<std::promise<unsigned>>(request.promise).get_future();
  std::scoped_lock lk(m_mtx);
  if (m_thread.joinable()) {
    m_requests.push(std::move(request));
    [[maybe_unused]] auto rc = eventfd_write(m_request_fd, 1);
    assert(rc == 0);
  } else {
    std::get<std::promise<unsigned>>(request.promise).set_value(0);
  }
  return result;
}

auto
FEngineBase::send(
  std::chrono::nanoseconds::rep t0,
  L::PhysicalRegion const& samples_region,
  std::function<void(unsigned)> const& cb) -> void {

  SendPackets request;
  request.t0 = std::chrono::nanoseconds{t0};
  request.samples_region = &samples_region;
  request.promise = cb;
  std::scoped_lock lk(m_mtx);
  if (m_thread.joinable()) {
    m_requests.push(std::move(request));
    [[maybe_unused]] auto rc = eventfd_write(m_request_fd, 1);
    assert(rc == 0);
  } else {
    cb(0);
  }
}

auto
FEngineBase::num_packets(L::PhysicalRegion const& samples_region) -> unsigned {
  return md_t::rect_t(samples_region).volume()
         / DataPacketConfiguration::volume;
}

auto
FEngineBase::split_multicast_option(std::string const& str)
  -> std::array<std::string, 2> {
  auto colon = str.find(':');
  if (colon == str.npos)
    throw std::runtime_error("FIXME");
  auto node = std::string(str.substr(0, colon));
  auto service = std::string(str.substr(colon + 1, str.npos - (colon + 1)));
  return {node, service};
}

auto
FEngineBase::parse_multicast_option(std::string const& str)
  -> std::tuple<std::string, std::uint16_t> {
  auto [node, service] = split_multicast_option(str);
  auto port = std::uint16_t(std::stoul(service));
  return {node, port};
}

SendFSamplesPartition::SendFSamplesPartition(
  L::Context ctx,
  L::Runtime* rt,
  receivers_def_t const& receivers_by_shard,
  unsigned max_fe_per_shard,
  index_space_t is)
  : m_ip(make_partition(ctx, rt, receivers_by_shard, max_fe_per_shard, is)) {}

auto
SendFSamplesPartition::index_partition() const -> md_t::index_partition_t {
  return m_ip;
}

auto
SendFSamplesPartition::receivers_by_shard_and_fe() const
  -> std::vector<std::vector<std::array<coord_t, 2>>> const& {
  return m_receivers_by_shard_and_fe;
}

auto
SendFSamplesPartition::make_partition(
  L::Context ctx,
  L::Runtime* rt,
  receivers_def_t const& receivers_by_shard,
  unsigned max_fe_per_shard,
  index_space_t fsamples_is) -> index_partition_t {

  auto bounds = rt->get_index_space_domain(fsamples_is).bounds;
  std::vector<L::Rect<2, coord_t>> cs_rects;
  std::map<L::Point<2, coord_t>, L::DomainT<md_t::dim, coord_t>> domains;
  m_receivers_by_shard_and_fe.reserve(receivers_by_shard.size());
  for (auto&& s :
       std::views::iota(coord_t{0}, coord_t(receivers_by_shard.size()))) {
    auto receivers_by_fe = rcp::block_distribute(
      coord_t{receivers_by_shard[s][1]},
      coord_t{max_fe_per_shard},
      coord_t{receivers_by_shard[s][0]});
    coord_t num_fe = receivers_by_fe.size();
    m_receivers_by_shard_and_fe.push_back(receivers_by_fe);
    cs_rects.push_back(L::Rect<2, coord_t>{{s, 0}, {s, num_fe - 1}});
    for (auto&& f : std::views::iota(coord_t{0}, num_fe)) {
      bounds.lo[dim(md_t::axes_t::receiver)] = receivers_by_fe[f][0];
      bounds.hi[dim(md_t::axes_t::receiver)] =
        bounds.lo[dim(md_t::axes_t::receiver)] + receivers_by_fe[f][1] - 1;
      domains[{s, f}] = md_t::domain_t(bounds);
    }
  }
  return rt->create_partition_by_domain(
    ctx,
    fsamples_is,
    domains,
    rt->create_index_space(ctx, cs_rects),
    true,
    LEGION_DISJOINT_COMPLETE_KIND);
}

SendFSamples::SendFSamples(
  L::Context ctx,
  L::Runtime* rt,
  Configuration const& config,
  std::size_t shard_point,
  receivers_def_t const& receivers_by_shard,
  fs_md_t::index_space_t index_space,
  L::MappingTagID tag)
  : m_samples_partition(SendFSamplesPartition(
      ctx, rt, receivers_by_shard, config.max_num_fe_per_shard, index_space)) {

  using fe_md_t = SendFSamplesTask::fe_md_t;

  // create a region for FEngine instance pointers; color space of
  // m_samples_partition is index by (shard, fe)
  auto is = SendFSamplesPartition::color_space_t(
    rt->get_index_partition_color_space_name(
      m_samples_partition.index_partition()));
  auto fs = rt->create_field_space(ctx);
  auto fa = rt->create_field_allocator(ctx, fs);
  fa.allocate_field(sizeof(fe_md_t::fe_ptr_t), fe_md_t::fe_ptr_fid);
  m_fe_lr = rt->create_logical_region(ctx, is, fs);
  {
    // map m_fe_lr by shard, and initialize FEngine instances
    auto ip = create_partition_by_slicing(ctx, rt, is, {{0, 1}});
    auto lp = rt->get_logical_partition(m_fe_lr, ip);
    auto infos = OFIUtil::find_fabric_interfaces(config.fabric_provider);
    assert(infos.size() > 0);
    auto const num_fe = coord_t(
      m_samples_partition.receivers_by_shard_and_fe()[shard_point].size());
    m_packet_blocks.reserve(num_fe);
    m_packet_mrs.reserve(num_fe);
    for (coord_t f = 0; f < num_fe; ++f) {
      m_packet_blocks.emplace_back(
        config.packet_block_size * sizeof(fpacket_type),
        default_init_allocator<char>());
      m_packet_mrs.emplace_back(
        m_packet_blocks.back().data(),
        config.packet_block_size * sizeof(fpacket_type));
      auto pool = std::make_shared<rcp::dc::single_block_unsynchronized_pool>(
        &m_packet_mrs.back());
      m_fes.push_back(FEngineFactory::create(config, pool, infos.front()));
      m_fes.back()->start();
    }
    // FIXME: use Legion external resource and attach_external_resources()
    L::RegionRequirement req(
      rt->get_logical_subregion_by_color(lp, shard_point),
      LEGION_READ_WRITE,
      LEGION_SIMULTANEOUS,
      m_fe_lr);
    req.add_field(fe_md_t::fe_ptr_fid);
    // maintain an instance to create copy restriction of FEngine pointers
    m_fe_pr = rt->map_region(ctx, req);
    auto fe_ptrs = fe_md_t::fe_ptrs<LEGION_READ_WRITE>(*m_fe_pr);
    for (auto&& f : std::views::iota(coord_t{0}, num_fe))
      fe_ptrs[L::Point<2, coord_t>{coord_t(shard_point), coord_t(f)}] =
        m_fes[f].get();
  }
  {
    // add requirements to fields in m_fe_lr partitioned by
    // m_samples_partition color space
    m_t0 = std::make_shared<typename decltype(m_t0)::element_type>();
    m_launcher = L::IndexTaskLauncher(
      SendFSamplesTask::task_id,
      is,
      L::UntypedBuffer(m_t0.get(), sizeof(*m_t0)),
      L::ArgumentMap(),
      L::Predicate::TRUE_PRED /*pred*/,
      false /*must*/,
      0 /*id*/,
      tag /*tag*/);
    m_launcher.region_requirements.resize(SendFSamplesTask::num_regions);
    m_fe_ip =
      rt->create_partition_by_blockify(ctx, is, L::Point<2, coord_t>{1, 1});
    auto lp = rt->get_logical_partition(m_fe_lr, m_fe_ip);
    L::RegionRequirement ptr_req(
      lp, 0, LEGION_READ_WRITE, LEGION_SIMULTANEOUS, m_fe_lr);
    ptr_req.add_field(fe_md_t::fe_ptr_fid);
    m_launcher.region_requirements[SendFSamplesTask::fe_ptr_region_index] =
      ptr_req;
  }
}

auto
SendFSamples::index_partition() const -> fs_md_t::index_partition_t {
  return m_samples_partition.index_partition();
}

auto
SendFSamples::destroy(L::Context ctx, L::Runtime* rt) -> void {
  m_last_launch.wait_all_results();
  for (auto&& fe : m_fes)
    fe->stop();
  rt->unmap_region(ctx, *m_fe_pr);
  rt->destroy_field_space(ctx, m_fe_lr.get_field_space());
  rt->destroy_logical_region(ctx, m_fe_lr);
}

auto
SendFSamples::launch(
  L::Context ctx,
  L::Runtime* rt,
  fs_md_t::LogicalRegion region,
  std::chrono::nanoseconds const& t0) -> L::FutureMap {

  *m_t0 = t0.count();
  auto lp = fs_md_t::LogicalPartition{
    rt->get_logical_partition(region, index_partition())};
  m_launcher.region_requirements[SendFSamplesTask::send_region_index] =
    lp.requirement(
      0,
      LEGION_READ_ONLY,
      LEGION_EXCLUSIVE,
      region,
      StaticFields{FSampleField{}});
  m_last_launch = rt->execute_index_space(ctx, m_launcher);
  return m_last_launch;
}

auto
SendFSamples::preregister(L::TaskID tid) -> void {

  PortableTask<SendFSamplesTask>::preregister_task_variants(tid);
}

} // namespace rcp::fsim

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
