# Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
# SPDX-License-Identifier: BSD-2-Clause-Patent
#
cmake_minimum_required(VERSION 3.23)
include(FsimVersionDetails.cmake)
project(Fsim
  VERSION ${Fsim_VER}
  DESCRIPTION "rcp F-engine simulator"
  HOMEPAGE_URL https://gitlab.com/dsa-2000/rcp/fsim)
set(FSIM_CDASH_SUBMIT_URL "" CACHE STRING "CDash submit URL")
set(FSIM_CDASH_NIGHTLY_START_TIME "09:00:00 UTC" CACHE STRING
  "CDash Nightly start time")
if(FSIM_CDASH_SUBMIT_URL AND
   CMAKE_SOURCE_DIR STREQUAL CMAKE_CURRENT_SOURCE_DIR)
  include(CTest)
else()
  file(REMOVE ${CMAKE_CURRENT_BINARY_DIR}/DartConfiguration.tcl)
  set(BUILD_TESTING ON CACHE BOOL "Build unit tests")
  if(BUILD_TESTING)
    enable_testing()
  endif()
endif()
enable_language(CXX)

include(GNUInstallDirs)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake")

include(CheckCXXCompilerFlag)

# Set a default build type if none was specified
set(default_build_type Release)
if(EXISTS "${CMAKE_SOURCE_DIR}/.git")
  set(default_build_type Debug)
endif()

if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
  message(STATUS "Setting build type to '${default_build_type}' as none was specified.")
  set(CMAKE_BUILD_TYPE "${default_build_type}" CACHE
    STRING "Choose the type of build." FORCE)
  # Set the possible values of build type for cmake-gui
  set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS
    "Debug" "Release" "MinSizeRel" "RelWithDebInfo")
endif()

# Set a default install prefix
if(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT
    AND NOT WIN32
    AND CMAKE_SOURCE_DIR STREQUAL CMAKE_CURRENT_SOURCE_DIR)
  set(CMAKE_INSTALL_PREFIX "/opt/martin/${PROJECT_NAME}" CACHE PATH "..." FORCE)
endif()

# Project wide setup
set(MIN_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD ${MIN_CXX_STANDARD} CACHE STRING "C++ language standard")
if(${CMAKE_CXX_STANDARD} LESS ${MIN_CXX_STANDARD})
  message(FATAL_ERROR
    "C++ language standard must not be less than ${MIN_CXX_STANDARD}")
endif()
set(CMAKE_CXX_STANDARD_REQUIRED ON CACHE BOOL "C++ language standard required")
set(CMAKE_CXX_EXTENSIONS OFF)

find_package(Bark 0.100.0)
find_package(Librcp 0.107.0)
find_package(Rcpdc 0.106.0 REQUIRED)
find_package(Boost 1.80 REQUIRED COMPONENTS program_options)

add_subdirectory(src)

# if(CMAKE_SOURCE_DIR STREQUAL CMAKE_CURRENT_SOURCE_DIR)
#   if(BUILD_TESTING)
#     add_subdirectory(tests)
#   endif()
# endif()

include(CMakePackageConfigHelpers)
write_basic_package_version_file(FsimConfigVersion.cmake
  VERSION ${Fsim_VER}
  COMPATIBILITY SameMajorVersion)
configure_package_config_file(${CMAKE_CURRENT_SOURCE_DIR}/Config.cmake.in
  ${CMAKE_CURRENT_BINARY_DIR}/FsimConfig.cmake
  INSTALL_DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/fsim)
install(FILES
  ${CMAKE_CURRENT_BINARY_DIR}/FsimConfig.cmake
  ${CMAKE_CURRENT_BINARY_DIR}/FsimConfigVersion.cmake
  DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/fsim)
install(EXPORT FsimTargets
  DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/fsim
  NAMESPACE Fsim::)
