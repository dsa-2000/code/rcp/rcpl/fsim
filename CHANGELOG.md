## v20241105.0.0
 (2024-11-05)

### New features (3 changes)

- [Implement fully configurable sample value distributions](https://gitlab.com/dsa-2000/rcp/fsim/-/commit/41a0b12935cddac476425f838299db03b09d343d)
- [Implement udp packet multicast](https://gitlab.com/dsa-2000/rcp/fsim/-/commit/a972ef64de175c59e1bf3620e4f9d6eb099fbf9b)
- [Add Configuration::multicast_packets](https://gitlab.com/dsa-2000/rcp/fsim/-/commit/f30a09713e48986bfec1ac1a2fa5586457db76b4)

### Feature changes (3 changes)

- [Update dependencies: librcp to 0.107.0, rcpdc to 0.106.0](https://gitlab.com/dsa-2000/rcp/fsim/-/commit/5b962d61bc822538d18b981508224d7dc4c32003)
- [Use Legion handshake in send task body for async work synchronization](https://gitlab.com/dsa-2000/rcp/fsim/-/commit/1bd5726c3a67390b3a5b7e99534066dcd00a5c9b)
- [Bump librcp, rcpdc dependencies to 0.106.0 and 0.105.1, respectively](https://gitlab.com/dsa-2000/rcp/fsim/-/commit/350773eeaa2ecff3b547efa9349ce83100e3e8b5)

## v20230612.0.1
 (2023-06-12)

### Other (1 change)

- [Add explicit dependency on librcp](dsa-2000/code/rcp/rcpl/fsim@9996c2a163270fa9a383c50541d5193de80ecef4)

## v20230612.0.0
 (2023-06-12)

### Other (1 change)

- Reset version number
